package com.streamride.mymoodstream.services;

import com.streamride.mymoodstream.models.VkAudio;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by andreyzakharov on 26.05.14.
 */
public class MusicContainer {

    private static MusicContainer mInstance;

    public static MusicContainer getInstance(){
        if(mInstance == null){
            mInstance = new MusicContainer();
        }
        return mInstance;
    }


    private List<VkAudio> musicList;



    public void setMusicList(List<VkAudio> musicList){
        if(this.musicList == null)
            this.musicList = new ArrayList<VkAudio>();
        this.musicList.clear();
        this.musicList.addAll(musicList);
    }

    public List<VkAudio> getMusicList(){
        return this.musicList;
    }

    public VkAudio getMusicByPosition(int position){
        if(musicList != null && musicList.size() > position)
            return musicList.get(position);
        return null;
    }

    public int incrementPosition(int position){
        position++;
        if (musicList != null && musicList.size() == position)
            position = 0;
        return position;
    }

    public int decrementPosition(int position){
        position--;
        if(musicList != null && position < 0)
            position = 0;
        return position;

    }

    public void shuffle() {
        if(musicList != null)
            Collections.shuffle(musicList);
    }
}
