package com.streamride.mymoodstream.services;

import android.content.*;
import android.os.*;
import com.streamride.mymoodstream.models.VkAudio;
import com.streamride.mymoodstream.utils.C;

/**
 * Created by andreyzakharov on 31.07.14.
 */
public class UIPlayerProcessor {


    private Context context;
    private PlayerStateListener playerStateListener;

    public void init(Context context, PlayerStateListener playerStateListener){
        this.context = context;
        this.playerStateListener = playerStateListener;
        initReceiver();

    }


    public void connectToPlayerService(){
        Intent intent = new Intent(context, PlayerService.class);
        if(PlayerService.state == C.PLAYER_STATES.STOPPED)
            context.startService(intent);
        if(!mBound)
            context.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }


    public void initReceiver(){
        IntentFilter filters = new IntentFilter();
        filters.addAction(C.ACTIONS.BUFFER);
        filters.addAction(C.ACTIONS.CURRENT_POSITION);
        filters.addAction(C.ACTIONS.NEW_SONG);
        filters.addAction(C.ACTIONS.PLAYER_STATE);
        context.registerReceiver(mMusicReceiver, filters);
    }


    public void destroy(){
        context.unregisterReceiver(mMusicReceiver);
        if(mBound)
            context.unbindService(mConnection);
    }

    public void resumeSong(){
        if (!mBound)
            return;
        Bundle bundle = new Bundle();
//        bundle.putInt(C.PLAYER.AUDIO_ID, audio_id);
        Message msg = Message.obtain(null, PlayerService.RESUME);
        msg.setData(bundle);
        try {
            mService.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void pauseSong(){
        if (!mBound)
            return;
        Bundle bundle = new Bundle();
        Message msg = Message.obtain(null, PlayerService.PAUSE);
        msg.setData(bundle);
        try {
            mService.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void playSong(int position, VkAudio vkAudio) {
        if (!mBound)
            return;
        Bundle bundle = new Bundle();
        bundle.putInt(C.PLAYER.AUDIO_ID, vkAudio.id);
        bundle.putInt(C.PLAYER.POSITION, position);
        Message msg = Message.obtain(null, PlayerService.PLAY);
        msg.setData(bundle);
        try {
            mService.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void stopPlayer(){
        if (!mBound)
            return;
        Bundle bundle = new Bundle();
        Message msg = Message.obtain(null, PlayerService.PAUSE);
        msg.setData(bundle);
        try {
            mService.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void setShuffle(){
        sendMessage(null, PlayerService.SHUFFLE);
    }


    public void nextSong(){
        sendMessage(null, PlayerService.NEXT);
    }

    public void prevSong(){
        sendMessage(null, PlayerService.PREV);
    }

    private void sendMessage(Bundle bundle, int what){
        if(!mBound)
            return;
        Message msg = Message.obtain(null,what);
        if(bundle != null)
            msg.setData(bundle);
        try {
            mService.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }


    public void seekSong(int progress){
        if (!mBound)
            return;
        Bundle bundle = new Bundle();
        bundle.putInt(C.PLAYER.POSITION, progress);
        Message msg = Message.obtain(null, PlayerService.SEEK);
        msg.setData(bundle);
        try {
            mService.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }


    private Messenger mService;
    private boolean mBound;
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mService = null;
            mBound = false;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = new Messenger(service);
            mBound = true;
            playerStateListener.onServiceConnected();
        }
    };


    BroadcastReceiver mMusicReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(C.ACTIONS.NEW_SONG.equals(intent.getAction())){
                playerStateListener.newSong();
            }
            else if(C.ACTIONS.BUFFER.equals(intent.getAction())){
                int buffer = intent.getIntExtra(C.PLAYER.BUFFER,0);
                playerStateListener.updatePlayerBuffer(buffer);
            }
            else if(C.ACTIONS.CURRENT_POSITION.equals(intent.getAction())){
                int curPosition = intent.getIntExtra(C.PLAYER.CURRENT_TIME,0);
                playerStateListener.updatePlayerPosition(curPosition);
            }
            else if(C.ACTIONS.PLAYER_STATE.equals(intent.getAction())){
                int state = intent.getIntExtra(C.PLAYER.STATE,0);
                playerStateListener.changePlayerState(state);
            }
        }
    };


    public interface PlayerStateListener {
        public void updatePlayerBuffer(int buffer);
        public void changePlayerState(int state);
        public void updatePlayerPosition(int position);
        public void newSong();
        public void onServiceConnected();
    }

}
