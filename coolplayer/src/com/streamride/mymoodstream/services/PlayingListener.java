package com.streamride.mymoodstream.services;

/**
 * Created by andreyzakharov on 12.04.14.
 */
public interface PlayingListener {

    public void pauseSong(int audioId);
    public void resumeSong(int audioId);
}
