package com.streamride.mymoodstream.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaExtractor;
import android.media.MediaPlayer;
import android.net.wifi.WifiManager;
import android.os.*;
import android.support.v4.app.NotificationCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.streamride.mymoodstream.models.VkAudio;
import com.streamride.moodstream.R;
import com.streamride.mymoodstream.ui.PlayerActivity;
import com.streamride.mymoodstream.utils.C;
import com.streamride.mymoodstream.utils.L;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by andreyzakharov on 03.04.14.
 */
public class PlayerService extends Service implements AudioManager.OnAudioFocusChangeListener {


    public static final int PLAY = 0;

    public static final int PAUSE = 1;
    public static final int RESUME =2;
    public static final int SEEK = 3;
    private static final String WIFI_LOCK_NAME = "wifilockname";
    public static final int NEXT = 4;
    public static final int PREV = 5;
    public static final int REPEAT = 6;
    public static final int SHUFFLE = 7;

    private MediaPlayer mPlayer;
    private int NOTIF_ID = 1;

    final Messenger mMessenger = new Messenger(new InComingHandler());

    private Notification notification;

    private Intent intent2open;

    public static int state = C.PLAYER_STATES.STOPPED;

//    private List<Integer> playList = new ArrayList<Integer>();
    public static int currentAudioId = 0;
    public static VkAudio currentSong;
    public static int currentPosition = 0;
    private Thread sendCurTime;
    private TelephonyManager telephonyManager;
    private WifiManager.WifiLock wifiLock;
    private AudioManager audioManager;
    private MediaExtractor mediaExtractor;
    private MediaPlayer mediaPlayer;
    private File mediaFile;
    private int counter=0;
    private final Handler handler = new Handler();
    private boolean isInterrupted;
    public static boolean mRepeat = false;
    boolean isNewSong = false;

    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        initNotif();

        wifiLock = ((WifiManager) getSystemService(Context.WIFI_SERVICE)).createWifiLock(
                WifiManager.WIFI_MODE_FULL, WIFI_LOCK_NAME);

        // сервис рекации на звонки
        telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        if (telephonyManager != null) {
            telephonyManager.listen(phoneStateListener,
                    PhoneStateListener.LISTEN_CALL_STATE);
        }

        initMediaPlayer();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        L.debug(" ondestroy service ");
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        loadTrack();
        return START_NOT_STICKY;
    }

    private void initMediaPlayer() {
        L.debug("INIT MEDIA PLAYER");
        mPlayer = new MediaPlayer();
        mPlayer.setOnPreparedListener(prepareListener);
        mPlayer.setOnCompletionListener(completionListener);
        mPlayer.setOnBufferingUpdateListener(bufferingListener);
        mPlayer.setOnErrorListener(errorListener);
        mPlayer.setWakeMode(getApplicationContext(),
                PowerManager.PARTIAL_WAKE_LOCK);
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                // resume playback
                if (mPlayer == null) initMediaPlayer();
                else if (!mPlayer.isPlaying() && state != C.PLAYER_STATES.PAUSED){
                    MP_Resume();
                }
                mPlayer.setVolume(1.0f, 1.0f);
                break;

            case AudioManager.AUDIOFOCUS_LOSS:
                // Lost focus for an unbounded amount of time: stop playback and
                // release media player
                if (mPlayer.isPlaying()) MP_Stop();
                mPlayer.release();
                mPlayer = null;
                break;

            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                // Lost focus for a short time, but we have to stop
                // playback. We don't release the media player because playback
                // is likely to resume
                if (mPlayer.isPlaying()){
                    MP_Pause();
                    setState(C.PLAYER_STATES.PAUSED);
                }
                break;

            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                // Lost focus for a short time, but it's ok to keep playing
                // at an attenuated level
                if (mPlayer.isPlaying()) mPlayer.setVolume(0.1f, 0.1f);
                break;
        }
    }


    class InComingHandler extends Handler {


        @Override
        public void handleMessage(Message msg) {
            L.debug(" handle message ");
            switch (msg.what) {
                case PLAY: {
                    currentPosition = msg.getData().getInt(C.PLAYER.POSITION);
                    stopSendingPosition();
                    loadTrack();
                    break;
                }
                case PAUSE: {
                    MP_Pause();
                    L.debug("pause in service");
                    stopSendingPosition();
                    break;
                }
                case RESUME: {
                    MP_Resume();
                    break;
                }
                case NEXT: {
                    currentPosition = MusicContainer.getInstance().incrementPosition(currentPosition);
//                    stopSendingPosition();
                    if(state == C.PLAYER_STATES.PREPARING)
                       isNewSong = true;
                    else
                        loadTrack();
                    break;
                }
                case PREV: {
                    currentPosition = MusicContainer.getInstance().decrementPosition(currentPosition);
                    if(state == C.PLAYER_STATES.PREPARING)
                        isNewSong = true;
                    else
                        loadTrack();
                    break;
                }
                case REPEAT: {
                    mRepeat = true;
                    break;
                }
                case SHUFFLE: {
                    MusicContainer.getInstance().shuffle();
                    break;
                }

                case SEEK: {
                    int progress = msg.getData().getInt(C.PLAYER.POSITION);
                    MP_Seek(progress);
                    break;
                }
                default:
                    super.handleMessage(msg);
            }
        }
    }

    private boolean isRepeat(){
        return mRepeat;
    }

    private void playSong(){
        stopSendingPosition();
        loadTrack();
    }

    MediaPlayer.OnPreparedListener prepareListener = new MediaPlayer.OnPreparedListener() {

        @Override
        public void onPrepared(MediaPlayer mp) {

            L.debug(" prepared ");
            if(isNewSong){
                L.debug(" new song ");
                loadTrack();
                isNewSong = false;
            }
            else {
                L.debug(" start ");
                mp.start();
                startSendingPosition();
                requestAudioFocus();
                setState(C.PLAYER_STATES.PLAYING);
                initNotif();
            }
        }
    };

    private void setNextPosition(){
        currentPosition = MusicContainer.getInstance().incrementPosition(currentPosition);
    }

    MediaPlayer.OnCompletionListener completionListener = new MediaPlayer.OnCompletionListener() {

        @Override
        public void onCompletion(MediaPlayer mp) {
//            removeNotification();
            if(!isRepeat())
                setNextPosition();
            loadTrack();
            wifiLockRelease();

        }
    };

    MediaPlayer.OnBufferingUpdateListener bufferingListener = new MediaPlayer.OnBufferingUpdateListener() {

        @Override
        public void onBufferingUpdate(MediaPlayer mp, int percent) {
            if(percent >= 0)
                sendBuffered(percent);
            L.debug(" buffering ");
        }
    };

    MediaPlayer.OnErrorListener errorListener = new MediaPlayer.OnErrorListener() {

        @Override
        public boolean onError(MediaPlayer mp, int what, int extra) {
            restartPlayer();
            setState(C.PLAYER_STATES.STOPPED);
            return false;
        }
    };

    private void sendBuffered (int b) {
        Intent intent = new Intent(C.ACTIONS.BUFFER);
        intent.putExtra(C.PLAYER.BUFFER, b);
        sendBroadcast(intent);
    }

    private void sendNewCurrentId(){
        Intent intent = new Intent(C.ACTIONS.NEW_SONG);
        intent.putExtra(C.PLAYER.AUDIO_ID, currentAudioId);
        sendBroadcast(intent);
    }

    private void loadTrack(){
        new GetSongFromDbByIdTask().execute(currentPosition);
    }

    private void MP_Pause(){
        L.debug("pause before");
        if(state == C.PLAYER_STATES.PLAYING){
            try {
                mPlayer.pause();
            }catch (IllegalStateException e){
                restartPlayer();
            }
            setState(C.PLAYER_STATES.PAUSED);

            L.debug("pause after");
            removeNotification();
        }
    }

    private void restartPlayer(){
        mPlayer.release();
        mPlayer = null;
        initMediaPlayer();
    }

    private void MP_Stop () {

        if (mPlayer != null) {
            if(mPlayer.isPlaying()){
                mPlayer.stop();
                setState(C.PLAYER_STATES.STOPPED);
            }
            abandonAudioFocus();
            mPlayer.reset();
            wifiLockRelease();
            sendPlayerState();
        }
    }

    private void MP_Resume(){
        L.debug("resume before");
        if(state == C.PLAYER_STATES.PAUSED){
            L.debug("resume after");
            try {
                mPlayer.start();
                setState(C.PLAYER_STATES.PLAYING);
                startSendingPosition();
                initNotif();
//                getNotificationManager().notify(NOTIF_ID, notification);
            }catch (IllegalStateException e){
                e.printStackTrace();
            }
        }
    }

    private void MP_Seek(int progress){
        if(mPlayer.isPlaying()){
            mPlayer.seekTo(progress*1000);
        }
    }

    private void startSendingPosition(){
        sendCurTime = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (mPlayer.isPlaying()) {
                        sendPlayingPosition();
                        try {
                            Thread.sleep(1);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                catch (IllegalStateException e){
                    e.printStackTrace();
                }
            }
        });
        sendCurTime.start();
    }

    private void stopSendingPosition(){
        if(sendCurTime != null && sendCurTime.isAlive())
            sendCurTime.interrupt();
    }

    private void sendPlayingPosition(){
        int currentPosition = mPlayer.getCurrentPosition();
        Intent intent = new Intent(C.ACTIONS.CURRENT_POSITION);
        intent.putExtra(C.PLAYER.CURRENT_TIME, currentPosition/1000);
        sendBroadcast(intent);
    }


    private void sendPlayerState(){
        Intent intent = new Intent(C.ACTIONS.PLAYER_STATE);
        intent.putExtra(C.PLAYER.STATE, state);
        sendBroadcast(intent);
    }


    private void MP_PlayTrack() {
        try {

            L.debug(" play track ");
            if (mPlayer.isPlaying()) {
                mPlayer.stop();
                L.debug(" stopped track ");

            }
            mPlayer.reset();
            L.debug(" reset ");
            wifiLockSet();
            L.debug(" wifi lock ");
            if(currentSong.isSaved){
                File mediaFile = new File(Environment.getExternalStorageDirectory(),  currentSong.artist + "-" + currentSong.title);
                if(!mediaFile.exists())
                    mediaFile = new File(getCacheDir(), currentSong.artist + "-" + currentSong.title);
                if(mediaFile.exists() && mediaFile.isFile()){
                    FileInputStream fileInputStream = new FileInputStream(mediaFile);
                    mPlayer.setDataSource(fileInputStream.getFD());
                }
                else
                    mPlayer.setDataSource(currentSong.url);
            }
            else {
                mPlayer.setDataSource(currentSong.url);
                L.debug(" set data source ");
            }


            mPlayer.prepareAsync();
            L.debug(" prepare async ");

        } catch (IllegalArgumentException e) {

            MP_Stop();
        } catch (SecurityException e) {
        } catch (IllegalStateException e) {

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void removeNotification(){

//        getNotificationManager().cancel(NOTIF_ID);
        stopForeground(true);
    }

    private void initNotif() {
//        intent2open = new Intent(this, PlayerActivity.class);
//        intent2open.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
//                | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
//                | Intent.FLAG_ACTIVITY_NEW_TASK);
//        intent2open.setAction("android.intent.action.VIEW");
////        intent2open.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent2open.setAction(Intent.ACTION_MAIN);
//        intent2open.addCategory(Intent.CATEGORY_LAUNCHER);
//
//        createNotif(currentSong.artist + " - " + currentSong.title);


        PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0,
                new Intent(getApplicationContext(), PlayerActivity.class),
                PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new Notification();
        notification.tickerText = getResources().getString(R.string.app_name);
        notification.icon = R.drawable.logo;
        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        notification.setLatestEventInfo(getApplicationContext(), getResources().getString(R .string.app_name),
                "Playing: " + currentSong.artist + " - " + currentSong.title, pi);


//        Notification nn = new Notification.Builder(this)
//                .setContentTitle(getResources().getString(R.string.app_name))
//                .setContentText(getResources().getString(R.string.app_name) + "Playing: " + currentSong.artist + " - " + currentSong.title)
//                .setSmallIcon(R.drawable.logo)




        startForeground(NOTIF_ID, notification);

    }

    private void createNotif(String subText) {
        NotificationCompat.Builder nb = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo)
                .setAutoCancel(false)
                .setTicker(getResources().getString(R .string.app_name))
                .setContentIntent(
                        PendingIntent.getActivity(this, 0, intent2open,
                                PendingIntent.FLAG_CANCEL_CURRENT))
                .setSmallIcon(R.drawable.logo)
                .setContentTitle(getString(R.string.playing)).setContentText(subText);
        notification = nb.build();
        notification.flags = notification.flags | Notification.FLAG_ONGOING_EVENT;
                ;
        startForeground(NOTIF_ID, notification);
//        getNotificationManager().notify(NOTIF_ID, notification);
    }

    private void requestAudioFocus(){
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int result = audioManager.requestAudioFocus(this,
                AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
    }

    private void abandonAudioFocus(){
        if(audioManager != null)
            audioManager.abandonAudioFocus(this);

    }

    private NotificationManager getNotificationManager(){
        return (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
    }

    private class GetSongFromDbByIdTask extends AsyncTask<Integer, Void, VkAudio>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                if (mPlayer.isPlaying()) {
                    stopSendingPosition();
                    MP_Pause();
                }
            }catch (IllegalStateException e){
                e.printStackTrace();
                restartPlayer();
            }
        }

        @Override
        protected VkAudio doInBackground(Integer... params) {
            return MusicContainer.getInstance().getMusicByPosition(params[0]);
        }

        @Override
        protected void onPostExecute(VkAudio vkAudio) {
            super.onPostExecute(vkAudio);
            if(vkAudio != null) {
                currentSong = vkAudio;
                currentAudioId = vkAudio.id;
                sendNewCurrentId();
                setState(C.PLAYER_STATES.PREPARING);
                MP_PlayTrack();
//                startPlayStrack(currentSong.url);
            }
        }
    }

    private void startPlayStrack(final String url){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    downloadIncrement(url);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    private void setState(int s){
        state = s;
        L.debug("state = " + s);
        sendPlayerState();
    }


    PhoneStateListener phoneStateListener = new PhoneStateListener() {



        @Override
        public void onCallStateChanged (int telState, String incomingNumber) {
            if (telState == TelephonyManager.CALL_STATE_RINGING) {
                // Incoming call: Pause music
                if(state == C.PLAYER_STATES.PLAYING){
                    MP_Pause();
                }

            } else if (telState == TelephonyManager.CALL_STATE_IDLE) {
                // Not in call: Play music
                L.debug("CALL STATE IDLE");
                L.debug(state);
                if (state == C.PLAYER_STATES.PAUSED) {
                    MP_Resume();
                }
            } else if (telState == TelephonyManager.CALL_STATE_OFFHOOK) {
                // A call is dialing, active or on hold
                if(state == C.PLAYER_STATES.PLAYING){
                    MP_Pause();
                }
            }
            super.onCallStateChanged(telState, incomingNumber);
        }
    };

    private void wifiLockSet () {
//        if(!isExit)
//            startForeground(NOTIF_ID, notification);
        if (!wifiLock.isHeld()) wifiLock.acquire();
    }

    private void wifiLockRelease () {
        stopForeground(true);
        if (wifiLock.isHeld()) wifiLock.release();
    }


    private void downloadIncrement(String url) throws IOException {
        URLConnection urlConnection = new URL(url).openConnection();
        urlConnection.connect();

        InputStream inputStream = urlConnection.getInputStream();

        if(inputStream == null){
            L.debug("null");
        }


        mediaFile = new File(getCacheDir(),"temp1ss.datt");
        if (mediaFile.exists()) {
            mediaFile.delete();
        }
        FileOutputStream outputStream = new FileOutputStream(mediaFile);

        byte[] buf = new byte[16384];
        int totalBytesReaded = 0, incrementBytesReaded = 0;

        do {
            int numread = inputStream.read(buf);

            if(numread <= 0)
                break;
            else {
                outputStream.write(buf,0,numread);
                totalBytesReaded += numread;
                incrementBytesReaded += numread;
                play();
            }
        } while(validateNotInterrupted());

        outputStream.close();
        inputStream.close();

    }

    private boolean validateNotInterrupted() {
        if (isInterrupted) {
            if (mediaPlayer != null) {
                mediaPlayer.pause();
                //mediaPlayer.release();
            }
            return false;
        } else {
            return true;
        }
    }

    private void fireDataFullyLoaded() {
        Runnable updater = new Runnable() {
            public void run() {
                transferBufferToMediaPlayer();

                // Delete the downloaded File as it's now been transferred to the currently playing buffer file.
            }
        };
        handler.post(updater);
    }

//    private void fireDataLoadUpdate() {
//        Runnable updater = new Runnable() {
//            public void run() {
//                float loadProgress = ((float)totalKbRead/(float)mediaLengthInKb);
//            }
//        };
////        handler.post(updater);
//    }

    private void play(){
        if(mediaPlayer == null) {
            try {
                File file = new File(getCacheDir(),"temp1" + (counter++) + ".datt");
                moveFile(mediaFile,file);
                mediaPlayer = createMediaPlayer(file);
                mediaPlayer.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            transferBufferToMediaPlayer();
        }
    }

    private MediaPlayer createMediaPlayer(File mediaFile)
            throws IOException {
        MediaPlayer mPlayer = new MediaPlayer();
        mPlayer.setOnErrorListener(
                new MediaPlayer.OnErrorListener() {
                    public boolean onError(MediaPlayer mp, int what, int extra) {
                        return false;
                    }
                });

        FileInputStream fis = new FileInputStream(mediaFile);
        mPlayer.setDataSource(fis.getFD());
        mPlayer.prepare();
        return mPlayer;
    }


    private void transferBufferToMediaPlayer() {
        try {
            // First determine if we need to restart the player after transferring data...e.g. perhaps the user pressed pause
            boolean wasPlaying = mediaPlayer.isPlaying();
            int curPosition = mediaPlayer.getCurrentPosition();

            // Copy the currently downloaded content to a new buffered File.  Store the old File for deleting later.
            File oldBufferedFile = new File(getCacheDir(),"temp1" + counter + ".datt");
            File bufferedFile = new File(getCacheDir(),"temp1" + (counter++) + ".datt");

            //  This may be the last buffered File so ask that it be delete on exit.  If it's already deleted, then this won't mean anything.  If you want to
            // keep and track fully downloaded files for later use, write caching code and please send me a copy.
            bufferedFile.deleteOnExit();
            moveFile(mediaFile,bufferedFile);

            // Pause the current player now as we are about to create and start a new one.  So far (Android v1.5),
            // this always happens so quickly that the user never realized we've stopped the player and started a new one
//            mediaPlayer.pause();

            // Create a new MediaPlayer rather than try to re-prepare the prior one.
            mediaPlayer = createMediaPlayer(bufferedFile);
            mediaPlayer.seekTo(curPosition);

            //  Restart if at end of prior buffered content or mediaPlayer was previously playing.
            //	NOTE:  We test for < 1second of data because the media player can stop when there is still
            //  a few milliseconds of data left to play
            boolean atEndOfFile = mediaPlayer.getDuration() - mediaPlayer.getCurrentPosition() <= 1000;
            if (wasPlaying || atEndOfFile){
                mediaPlayer.start();
            }

            // Lastly delete the previously playing buffered File as it's no longer needed.
            oldBufferedFile.delete();

        }catch (Exception e) {
            Log.e(getClass().getName(), "Error updating to newly loaded content.", e);
        }
    }

    public void moveFile(File	oldLocation, File	newLocation)
            throws IOException {

        if ( oldLocation.exists( )) {
            BufferedInputStream  reader = new BufferedInputStream( new FileInputStream(oldLocation) );
            BufferedOutputStream  writer = new BufferedOutputStream( new FileOutputStream(newLocation, false));
            try {
                byte[]  buff = new byte[8192];
                int numChars;
                while ( (numChars = reader.read(  buff, 0, buff.length ) ) != -1) {
                    writer.write( buff, 0, numChars );
                }
            } catch( IOException ex ) {
                throw new IOException("IOException when transferring " + oldLocation.getPath() + " to " + newLocation.getPath());
            } finally {
                try {
                    if ( reader != null ){
                        writer.close();
                        reader.close();
                    }
                } catch( IOException ex ){
                    Log.e(getClass().getName(),"Error closing files when transferring " + oldLocation.getPath() + " to " + newLocation.getPath() );
                }
            }
        } else {
            throw new IOException("Old location does not exist when transferring " + oldLocation.getPath() + " to " + newLocation.getPath() );
        }
    }

}
