package com.streamride.mymoodstream.widgets;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import com.appsflyer.AppsFlyerLib;

/**
 * Created by andreyzakharov on 25.06.14.
 */
public class AdsWebView extends WebView{

    public AdsWebView(Context context) {
        super(context);
        init();
    }

    public AdsWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AdsWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }


    private void init(){
//        setWebViewClient(new WebViewClient(){
//
//            @Override
//            public void onLoadResource(WebView view, String url) {
//                if (!url.contains("show_app_ad.js") && !url.contains(".css")
//                        && (url.startsWith("http://") || url.startsWith("https://") || url.startsWith("market://"))) {
//
//                    WebView.HitTestResult hitTestResult = getHitTestResult();
//                    if (hitTestResult != null && hitTestResult.getType() > 0) {
//                        getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
//
//                       stopLoading();
//                    }
//                }
//            }
//
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
//                return false;
//            }
//        });

        getSettings().setSupportMultipleWindows(true);

        setWebChromeClient(new WebChromeClient() {

            @Override
            public boolean onCreateWindow(WebView view, boolean dialog, boolean userGesture, android.os.Message resultMsg) {
                AppsFlyerLib.sendTrackingWithEvent(getContext(), "try_to_open_ad", "");
                if (userGesture) {
                    try {
                        WebView.HitTestResult result = view.getHitTestResult();
                        if (result.getType() == WebView.HitTestResult.SRC_IMAGE_ANCHOR_TYPE) {
                            String data = result.getExtra();
                            Context context = view.getContext();
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(data));
                            context.startActivity(browserIntent);
//                            adsWv.setWebViewClient(new WebViewClient(){
//                                @Override
//                                public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                                    mUrl = url;
//                                    adsWv.setWebViewClient(null);
//                                    String data = url;
//                                    Context context = view.getContext();
//                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(data));
//                                    context.startActivity(browserIntent);
//                                    return false;
//                                }
//                            });
                        }
//                        else{
//                            String data = result.getExtra();
//                            Context context = view.getContext();
//                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(data));
//                            context.startActivity(browserIntent);
//                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return false;
                }

                return false;
            }
        });



    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN){

            int temp_ScrollY = getScrollY();
            scrollTo(getScrollX(), getScrollY() + 1);
            scrollTo(getScrollX(), temp_ScrollY);

        }

        return super.onTouchEvent(ev);
    }
}
