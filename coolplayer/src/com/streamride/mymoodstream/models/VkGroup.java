package com.streamride.mymoodstream.models;

import android.content.ContentValues;
import android.database.Cursor;
import com.streamride.mymoodstream.utils.C;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andreyzakharov on 31.07.14.
 */
public class VkGroup {


    public static final String NAME = "name";
    public static final String ID = "id";
    public static final String PHOTO_50 = "photo_50";
    public static final String PHOTO_100 = "photo_100";
    public static final String PHOTO_200 = "photo_200";



    public String name;
    public int id;
    public String photo_50;
    public String photo_100;
    public String photo_200;



    public static VkGroup parse(JSONObject jsonObject){
        VkGroup group = new VkGroup();
        if(!jsonObject.isNull("id"))
            group.id = jsonObject.optInt("id");
        if(!jsonObject.isNull("name"))
            group.name = jsonObject.optString("name");
        if(!jsonObject.isNull("photo_50"))
            group.photo_50 = jsonObject.optString("photo_50");
        if(!jsonObject.isNull("photo_100"))
            group.photo_100 = jsonObject.optString("photo_100");
        if(!jsonObject.isNull("photo_200"))
            group.photo_200 = jsonObject.optString("photo_200");

        return group;
    }


    public static ContentValues[] getContentValues(List<VkGroup> groupList){
        if(groupList != null){

            int size = groupList.size();
            ContentValues[] valuesList = new ContentValues[size];
            for(int i=0;i<size;i++){
                VkGroup vkGroup = groupList.get(i);
                valuesList[i] = getContentValuesForOne(vkGroup);
            }
            return valuesList;
        }
        return null;
    }

    public static ContentValues getContentValuesForOne(VkGroup vkGroup){
        ContentValues values = new ContentValues();
        values.put(ID, vkGroup.id);
        values.put(NAME, vkGroup.name);
        values.put(PHOTO_50, vkGroup.photo_50);
        values.put(PHOTO_100, vkGroup.photo_100);
        values.put(PHOTO_200, vkGroup.photo_200);
        return values;
    }

    public static List<VkGroup> initFromCursor(Cursor cursor){
        List<VkGroup> vkGroupList = new ArrayList<VkGroup>();
        if(cursor != null && cursor.moveToFirst()){
            do {
                VkGroup vkGroup = new VkGroup();
                int id = cursor.getInt(cursor.getColumnIndex(ID));
                String name = cursor.getString(cursor.getColumnIndex(NAME));
                String photo_50 = cursor.getString(cursor.getColumnIndex(PHOTO_50));
                String photo_100 = cursor.getString(cursor.getColumnIndex(PHOTO_100));
                String photo_200 = cursor.getString(cursor.getColumnIndex(PHOTO_200));

                vkGroup.name = name;
                vkGroup.id = id;
                vkGroup.photo_50 = photo_50;
                vkGroup.photo_100 = photo_100;
                vkGroup.photo_200 = photo_200;
                vkGroupList.add(vkGroup);
            } while (cursor.moveToNext());
        }


        return vkGroupList;
    }


}
