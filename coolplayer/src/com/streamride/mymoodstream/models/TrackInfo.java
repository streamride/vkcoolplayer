package com.streamride.mymoodstream.models;

import com.streamride.mymoodstream.utils.L;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by andreyzakharov on 22.05.14.
 */
public class TrackInfo implements Serializable{


    public String artist;
    public String albumTitle;
    public String imageSmall;
    public String imageMedium;
    public String imageLarge;
    public String imageExtraLarge;
    public String lyrics;




    public static TrackInfo parseFromJson(JSONObject jsonObject){
        if(jsonObject != null){
            L.debug(jsonObject);

            if(jsonObject.has("track")){
                try {
                    JSONObject track = jsonObject.getJSONObject("track");
                    TrackInfo trackInfo = new TrackInfo();
                    if(track.has("album")) {
                        JSONObject album = track.getJSONObject("album");
                        L.debug(album);
                        trackInfo.artist = album.getString("artist");
                        trackInfo.albumTitle = album.getString("title");
                        if (album.has("image")) {
                            JSONArray imageArray = album.getJSONArray("image");
                            for (int i = 0; i < imageArray.length(); i++) {
                                JSONObject object = imageArray.getJSONObject(i);
                                String size = object.getString("size");
                                if ("small".equals(size))
                                    trackInfo.imageSmall = object.getString("#text");
                                else if ("medium".equals(size))
                                    trackInfo.imageMedium = object.getString("#text");
                                else if ("large".equals(size))
                                    trackInfo.imageLarge = object.getString("#text");
                                else if ("extralarge".equals(size))
                                    trackInfo.imageExtraLarge = object.getString("#text");
                            }
                        }
                    }
                    return trackInfo;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public static TrackInfo parseLyrics(TrackInfo trackInfo, JSONObject jsonObject){
        if(trackInfo == null)
            trackInfo = new TrackInfo();
        if(jsonObject.has("response")){
            try {
                JSONObject response = jsonObject.getJSONObject("response");
                trackInfo.lyrics = response.getString("text");
                return trackInfo;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
