package com.streamride.mymoodstream.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.streamride.mymoodstream.utils.C;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by andreyzakharov on 03.04.14.
 */
public class VkAudio implements Serializable {

    public enum PLAYING_STATE {
        playing, paused, stopped
    }

    public static final String ID = "id";

    public static final String OWNER_ID = "owner_id";

    public static final String ARTIST = "artist";

    public static final String TITLE = "title";

    public static final String DURATION = "duration";

    public static final String URL = "url";

    public static final String LYRICS_ID = "lyrics_id";

    public static final String ALBUM_ID = "album_id";

    public static final String GENRE = "genre";

    public static final String PLAYLIST = "playlist";

    /**
     * Audio ID.
     */
    public int id;

    /**
     * Audio owner ID.
     */
    public int owner_id;

    /**
     * Artist name.
     */
    public String artist;

    /**
     * Audio file title.
     */
    public String title;

    /**
     * Duration (in seconds).
     */
    public int duration;

    /**
     * Link to mp3.
     */
    public String url;

    /**
     * ID of the lyrics (if available) of the audio file.
     */
    public int lyrics_id;

    /**
     * ID of the album containing the audio file (if assigned).
     */
    public int album_id;

    /**
     * Genre ID. See the list of audio genres.
     */
    public int genre;

    public String playlist_name;

    public boolean isSaved = false;

    private PLAYING_STATE playingState = PLAYING_STATE.stopped;

    public VkAudio(){

    }

    public PLAYING_STATE getPlayingState(){
        return playingState;
    }

    public void setPlayingState(PLAYING_STATE state){
        playingState  = state;
    }

    public VkAudio parse(JSONObject from, String playlist_name) {
        id = from.optInt("id");
        owner_id = from.optInt("owner_id");
        artist = from.optString("artist");
        title = from.optString("title");
        duration = from.optInt("duration");
        url = from.optString("url");
        lyrics_id = from.optInt("lyrics_id");
        album_id = from.optInt("album_id");
        genre = from.optInt("genre_id");
        this.playlist_name = playlist_name;
        return this;
    }

    public VkAudio parse(JSONObject from){
        id = from.optInt("id");
        owner_id = from.optInt("owner_id");
        artist = from.optString("artist");
        title = from.optString("title");
        duration = from.optInt("duration");
        url = from.optString("url");
        lyrics_id = from.optInt("lyrics_id");
        album_id = from.optInt("album_id");
        genre = from.optInt("genre_id");
        return this;
    }


    public static ContentValues[] getContentValues(List<VkAudio> songList) {
        if(songList != null){

            int size = songList.size();
            ContentValues[] valuesList = new ContentValues[size];
            for(int i=0;i<size;i++){
//                ContentValues values = new ContentValues();
                VkAudio vkAudio = songList.get(i);
//                values.put(ID, vkAudio.id);
//                values.put(OWNER_ID, vkAudio.owner_id);
//                values.put(ALBUM_ID, vkAudio.album_id);
//                values.put(ARTIST, vkAudio.artist);
//                values.put(DURATION, vkAudio.duration);
//                values.put(GENRE, vkAudio.genre);
//                values.put(TITLE, vkAudio.title);
//                values.put(URL, vkAudio.url);
//                values.put(LYRICS_ID, vkAudio.lyrics_id);
//                values.put(PLAYLIST, vkAudio.playlist_name);
                valuesList[i] = getContentValuesForOne(vkAudio);
            }
            return valuesList;
        }

        return null;
    }

    public static ContentValues getContentValuesForOne(VkAudio vkAudio){
        ContentValues values = new ContentValues();
        values.put(ID, vkAudio.id);
        values.put(OWNER_ID, vkAudio.owner_id);
        values.put(ALBUM_ID, vkAudio.album_id);
        values.put(ARTIST, vkAudio.artist);
        values.put(DURATION, vkAudio.duration);
        values.put(GENRE, vkAudio.genre);
        values.put(TITLE, vkAudio.title);
        values.put(URL, vkAudio.url);
        values.put(LYRICS_ID, vkAudio.lyrics_id);
        values.put(PLAYLIST, vkAudio.playlist_name);
        return values;
    }

    public static List<VkAudio> initFromCursor(Context context, Cursor cursor){
        List<VkAudio> vkAudioList = new ArrayList<VkAudio>();
        if(cursor != null && cursor.moveToFirst()){
            do {
                VkAudio vkAudio = new VkAudio();
                int id = cursor.getInt(cursor.getColumnIndex(ID));
                int owner_id = cursor.getInt(cursor.getColumnIndex(OWNER_ID));
                int album_id = cursor.getInt(cursor.getColumnIndex(ALBUM_ID));
                int duration = cursor.getInt(cursor.getColumnIndex(DURATION));
                int genre = cursor.getInt(cursor.getColumnIndex(GENRE));
                String title = cursor.getString(cursor.getColumnIndex(TITLE));
                String url = cursor.getString(cursor.getColumnIndex(URL));
                int lyrics_id = cursor.getInt(cursor.getColumnIndex(LYRICS_ID));
                String artist = cursor.getString(cursor.getColumnIndex(ARTIST));
                String playList = cursor.getString(cursor.getColumnIndex(PLAYLIST));

                vkAudio.album_id = album_id;
                vkAudio.artist = artist;
                vkAudio.id = id;
                vkAudio.duration = duration;
                vkAudio.url = url;
                vkAudio.genre = genre;
                vkAudio.lyrics_id = lyrics_id;
                vkAudio.title = title;
                vkAudio.owner_id = owner_id;
                vkAudio.playlist_name = playList;
                if(C.PLAYLIST.SAVED.equals(playList))
                    vkAudio.isSaved = true;

                vkAudioList.add(vkAudio);
            } while (cursor.moveToNext());
        }




        return vkAudioList;
    }

    @Override
    public boolean equals(Object o) {
        return id == ((VkAudio)o).id;
    }
}
