package com.streamride.mymoodstream.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andreyzakharov on 07.04.14.
 */
public class VkAlbum {


    public static final String OWNER_ID = "owner_id";

    public static final String TITLE = "title";

    public static final String ALBUM_ID = "album_id";


    public int owner_id;

    public String title;

    public int album_id;


    public VkAlbum parse(JSONObject jsonObject){
        album_id = jsonObject.optInt("id");
        title = jsonObject.optString("title");
        owner_id = jsonObject.optInt("owner_id");
        return this;
    }


    public static ContentValues[] getContentValues(List<VkAlbum> albumList) {
        if(albumList != null){
            int size = albumList.size();
            ContentValues[] valuesList = new ContentValues[size];
            for(int i=0;i<size;i++){
                ContentValues values = new ContentValues();
                VkAlbum vkAlbum = albumList.get(i);
                values.put(OWNER_ID, vkAlbum.owner_id);
                values.put(ALBUM_ID, vkAlbum.album_id);
                values.put(TITLE, vkAlbum.title);
                valuesList[i] = values;
            }
            return valuesList;
        }

        return null;
    }

    public static List<VkAlbum> initFromCursor(Context context, Cursor cursor){
        List<VkAlbum> vkAlbumList = new ArrayList<VkAlbum>();
        if(cursor != null && cursor.moveToFirst()){
            do {
                VkAlbum vkAlbum = new VkAlbum();
                int owner_id = cursor.getInt(cursor.getColumnIndex(OWNER_ID));
                int album_id = cursor.getInt(cursor.getColumnIndex(ALBUM_ID));
                String title = cursor.getString(cursor.getColumnIndex(TITLE));

                vkAlbum.album_id = album_id;
                vkAlbum.title = title;
                vkAlbum.owner_id = owner_id;

                vkAlbumList.add(vkAlbum);
            } while (cursor.moveToNext());
        }




        return vkAlbumList;
    }
}
