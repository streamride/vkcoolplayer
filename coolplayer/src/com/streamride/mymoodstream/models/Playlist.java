package com.streamride.mymoodstream.models;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by andreyzakharov on 30.04.14.
 */
public class Playlist {

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String TYPE = "type";
    public static final String IMG = "img";

    public String name;

    public String type;

    public String img;



//    public ParseObject createParseObject(){
//        ParseObject parseObject = new ParseObject("playlist");
//        parseObject.put(ID, 0);
//        parseObject.put(NAME, name);
//        parseObject.put(TYPE, type);
//        parseObject.put(IMG, img);
//
//        return parseObject;
//    }
//
//
//    public static Playlist getFromParse(ParseObject parseObject){
//        Playlist playlist = new Playlist();
//        if(parseObject != null){
//            playlist.img = parseObject.getString(IMG);
//            playlist.name = parseObject.getString(NAME);
//            playlist.type = parseObject.getString(TYPE);
//        }
//
//        return playlist;
//    }
//
//    public static List<Playlist> getListFromParse(List<ParseObject> parseObjectList){
//        List<Playlist> playlistList = new ArrayList<Playlist>();
//        for(ParseObject parseObject : parseObjectList){
//            playlistList.add(getFromParse(parseObject));
//        }
//
//        return playlistList;
//    }

}
