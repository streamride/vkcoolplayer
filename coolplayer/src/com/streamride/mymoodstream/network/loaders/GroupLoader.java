package com.streamride.mymoodstream.network.loaders;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.AsyncTaskLoader;
import android.widget.SimpleCursorAdapter;
import com.streamride.mymoodstream.db.DBContentProvider;
import com.streamride.mymoodstream.models.VkGroup;
import com.streamride.mymoodstream.utils.QueryBuilder;

import java.util.List;

/**
 * Created by andreyzakharov on 31.07.14.
 */
public class GroupLoader extends BaseLoader<List<VkGroup>> {


    public GroupLoader(Context context, Bundle bundle) {
        super(context, bundle);
    }

    @Override
    protected List<VkGroup> getDataFromDb() {
        QueryBuilder queryBuilder = new QueryBuilder();
        Cursor cursor = queryBuilder.query(getContext(), DBContentProvider.CONTENT_URI_GROUP);
        List<VkGroup> vkGroupList = VkGroup.initFromCursor(cursor);
        return vkGroupList;
    }
}
