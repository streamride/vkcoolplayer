package com.streamride.mymoodstream.network.loaders;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.AsyncTaskLoader;
import com.streamride.mymoodstream.db.DBContentProvider;
import com.streamride.mymoodstream.models.VkAlbum;
import com.streamride.mymoodstream.network.impl.AlbumResponse;

import java.util.List;

/**
 * Created by andreyzakharov on 21.07.14.
 */
public class AlbumLoader extends AsyncTaskLoader<List<VkAlbum>> {

    private Bundle mBundle;

    public AlbumLoader(Context context, Bundle bundle) {
        super(context);
        setBundle(bundle);
    }

    public Bundle getBundle() {
        return mBundle;
    }

    public void setBundle(Bundle bundle) {
        mBundle = bundle;
    }

    @Override
    public List<VkAlbum> loadInBackground() {
        final List<VkAlbum> firstFromDb = getDataFromDB(getContext());
        return firstFromDb;
    }

    private List<VkAlbum> getDataFromDB(Context context){
        String where = "";
        Cursor cursor = context.getContentResolver().query(DBContentProvider.CONTENT_URI_ALBUM,null,null,null,null);
        AlbumResponse albumResponse = new AlbumResponse();
        albumResponse.setResultList(VkAlbum.initFromCursor(context, cursor));
        return albumResponse.getResultList();
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }
}
