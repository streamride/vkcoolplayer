package com.streamride.mymoodstream.network.loaders;

import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.AsyncTaskLoader;

/**
 * Created by andreyzakharov on 31.07.14.
 */
public abstract class BaseLoader<T> extends AsyncTaskLoader<T> {

    private Bundle mBundle;

    public BaseLoader(Context context) {
        super(context);
    }

    public BaseLoader(Context context, Bundle bundle){
        this(context);
        setBundle(bundle);
    }

    public Bundle getBundle() {
        return mBundle;
    }

    public void setBundle(Bundle bundle) {
        mBundle = bundle;
    }

    @Override
    public T loadInBackground() {
        return getDataFromDb();
    }


    protected abstract T getDataFromDb();


    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }
}
