package com.streamride.mymoodstream.network.loaders;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.AsyncTaskLoader;
import com.streamride.mymoodstream.db.DBContentProvider;
import com.streamride.mymoodstream.models.VkAudio;
import com.streamride.mymoodstream.network.impl.AudioResponse;
import com.streamride.mymoodstream.utils.C;
import com.streamride.mymoodstream.utils.L;
import com.streamride.mymoodstream.utils.QueryBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andreyzakharov on 21.07.14.
 */
public class MusicLoader extends AsyncTaskLoader<List<VkAudio>> {

    private int albumId;
    private String playlist;
    private Bundle mBundle;

    public MusicLoader(Context context) {
        super(context);
    }

    public Bundle getBundle() {
        return mBundle;
    }

    public void setBundle(Bundle bundle) {
        mBundle = bundle;
    }

    public MusicLoader(Context context, String playlist, int albumId, Bundle bundle){
        this(context);
        this.playlist = playlist;
        this.albumId = albumId;
        setBundle(bundle);
    }

    @Override
    public List<VkAudio> loadInBackground() {
        final List<VkAudio> firstFromDb = getDataFromDB(getContext(), playlist);
        return firstFromDb;
    }

    private List<VkAudio> getDataFromDB(Context context, String playlist){
        if(C.PLAYLIST.SAVED.equals(playlist))
            return getSavedAudios();
        String where = VkAudio.PLAYLIST + " = " + "'" + playlist + "'";
        if(C.PLAYLIST.ALBUM_ID.equals(playlist))
            where += " AND " + VkAudio.ALBUM_ID + " = " + albumId;
        Cursor cursor = context.getContentResolver().query(DBContentProvider.CONTENT_URI_AUDIO,null,where,null,null);
        AudioResponse audioResponse = new AudioResponse();
        List<VkAudio> vkAudioList = VkAudio.initFromCursor(context, cursor);
        getSavedAudios(vkAudioList);
        audioResponse.setResultList(vkAudioList);
        if(cursor != null)
            cursor.close();
        L.debug(audioResponse);
        return audioResponse.getResultList();
    }

    private List<VkAudio> getSavedAudios(){
        QueryBuilder queryBuilder = new QueryBuilder();
        Cursor cursor = queryBuilder.query(getContext(), DBContentProvider.CONTENT_URI_SAVED);
        AudioResponse audioResponse = new AudioResponse();
        List<VkAudio> vkAudioList = VkAudio.initFromCursor(getContext(), cursor);
        audioResponse.setResultList(vkAudioList);
        return audioResponse.getResultList();
    }

    private void getSavedAudios(List<VkAudio> vkAudios){
        if(vkAudios != null && vkAudios.size() > 0) {
            Cursor cursor = getContext().getContentResolver().query(DBContentProvider.CONTENT_URI_SAVED, null, null, null, null);
            List<Integer> savedList = new ArrayList<Integer>();
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    int id = cursor.getInt(cursor.getColumnIndex(VkAudio.ID));
                    savedList.add(id);
                } while (cursor.moveToNext());
            }

            for(int i=0;i<vkAudios.size();i++){
                for(int j=0; j< savedList.size();j++) {
                    if (vkAudios.get(i).id == savedList.get(j)){
                        vkAudios.get(i).isSaved = true;
                    }
                }
            }
        }
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }
}
