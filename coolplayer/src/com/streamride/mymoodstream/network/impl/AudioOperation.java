package com.streamride.mymoodstream.network.impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import com.streamride.mymoodstream.db.DBContentProvider;
import com.streamride.mymoodstream.models.VkAudio;
import com.streamride.mymoodstream.network.HttpUtil;
import com.streamride.mymoodstream.network.OnRequestListener;
import com.streamride.mymoodstream.network.Operation;
import com.streamride.mymoodstream.parsers.MusicListParser;
import com.streamride.mymoodstream.utils.C;
import com.streamride.mymoodstream.utils.InternetStatus;
import com.streamride.mymoodstream.utils.L;
import org.json.JSONException;

import java.io.IOException;
import java.util.List;

/**
 * Created by andreyzakharov on 05.04.14.
 */
public class AudioOperation extends Operation {


    private final Context context;
    private final String playlist;
    private final int albumId;
    private List<VkAudio> songList;
    private AudioResponse audioResponse;

    public AudioOperation(Context context, String url, String playList, int album_id, OnRequestListener onRequestListener) {
        super(url, onRequestListener);
        this.context = context;
        this.playlist = playList;
        this.albumId = album_id;
    }

    @Override
    protected void start() {
        HttpUtil httpUtil = new HttpUtil();
        InternetStatus internetStatus = new InternetStatus(context);
        try {
            final List<VkAudio> firstFromDb = getDataFromDB(context, playlist);
            postInMainQueue(new Runnable() {
                @Override
                public void run() {
                    onRequestListener.onDbComplete(firstFromDb);
                }
            });
            if(internetStatus.isOnline()) {
                final String response = httpUtil.get(mUrl);
                parse(response);
                deleteDataFromDB();
                saveDataToDB();
                final List<VkAudio> audioList = getDataFromDB(context, playlist);
                postInMainQueue(new Runnable() {
                    @Override
                    public void run() {
                        onRequestListener.onNetworkComplete(audioList);
                    }
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void parse(String response) {
        MusicListParser parser = new MusicListParser();
        try {
            audioResponse = parser.parse(response, playlist);
            L.debug(audioResponse);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void deleteDataFromDB(){
        String where = VkAudio.PLAYLIST + " = " + "'" + playlist + "'";
        try {
            context.getContentResolver().delete(DBContentProvider.CONTENT_URI_AUDIO, where, null);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void saveDataToDB() {
        int result = 0;
        if(audioResponse.getResultList() != null){
            ContentValues[] contentValues = VkAudio.getContentValues(audioResponse.getResultList());
            Log.d("", contentValues.toString());
            result = context.getContentResolver().bulkInsert(DBContentProvider.CONTENT_URI_AUDIO, contentValues);
            L.debug(result);
        }

        if(result == 0){
            onRequestListener.onError();
        }
    }

    private List<VkAudio> getDataFromDB(Context context, String playlist){
        String where = VkAudio.PLAYLIST + " = " + "'" + playlist + "'";
        if(C.PLAYLIST.ALBUM_ID.equals(playlist))
            where += " AND " + VkAudio.ALBUM_ID + " = " + albumId;
        Cursor cursor = context.getContentResolver().query(DBContentProvider.CONTENT_URI_AUDIO,null,where,null,null);
        AudioResponse audioResponse = new AudioResponse();
        audioResponse.setResultList(VkAudio.initFromCursor(context, cursor));
        L.debug(audioResponse);
        return audioResponse.getResultList();
    }
}
