package com.streamride.mymoodstream.network.impl;

import com.streamride.mymoodstream.models.VkAlbum;
import com.streamride.mymoodstream.network.Response;

/**
 * Created by andreyzakharov on 10.04.14.
 */
public class AlbumResponse extends Response<VkAlbum> {

    public int count;
}
