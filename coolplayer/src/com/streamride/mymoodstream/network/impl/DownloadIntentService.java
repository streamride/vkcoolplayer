package com.streamride.mymoodstream.network.impl;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.streamride.moodstream.R;
import com.streamride.mymoodstream.db.DBContentProvider;
import com.streamride.mymoodstream.models.VkAudio;
import com.streamride.mymoodstream.network.HttpUtil;
import com.streamride.mymoodstream.ui.MainActivity;
import com.streamride.mymoodstream.utils.C;
import com.streamride.mymoodstream.utils.InternetStatus;
import com.streamride.mymoodstream.utils.L;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

/**
 * Created by andreyzakharov on 21.05.14.
 */
public class DownloadIntentService extends IntentService {
    private VkAudio item;
    private int mId;
    private int fileLength;
    private int total;


    public DownloadIntentService() {
        super("downloadservice");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        item = (VkAudio) intent.getSerializableExtra("vkaudio");
        download();
    }


    private void download(){
        L.debug("download started");
        HttpUtil httpUtil = new HttpUtil();
        InternetStatus internetStatus = new InternetStatus(this);
        if(internetStatus.isOnline()){

            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            OkHttpClient client = new OkHttpClient();
            try {
                URL url = new URL(item.url);
                connection = client.open(url);
                connection.connect();
                L.debug("connected");
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return;
                }



                // download the file
                input = connection.getInputStream();
                fileLength = connection.getContentLength();
                L.debug(fileLength);
                showNotify();
                File file = null;
                if(Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED && Environment.getExternalStorageState() != Environment.MEDIA_MOUNTED_READ_ONLY){
                    file = new File(Environment.getExternalStorageDirectory(), item.artist + "-" + item.title);
                }
                else
                    file = new File(getCacheDir(), item.artist + "-" + item.title);
                output = new FileOutputStream(file);

                byte data[] = new byte[4096];
                total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    
                    output.write(data, 0, count);
                }
                setSongSaved();
            } catch (Exception e) {
                L.debug(e.toString());
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }
            return;
        }
    }


    private void setSongSaved(){
        item.playlist_name = C.PLAYLIST.SAVED;
        ContentValues contentValues = VkAudio.getContentValuesForOne(item);
        getContentResolver().insert(DBContentProvider.CONTENT_URI_SAVED, contentValues);
        Intent intent = new Intent(C.ACTIONS.SONG_DOWNLOADED);
        intent.putExtra(VkAudio.ID, item.id);
        sendBroadcast(intent);
    }

    private void showNotify(){
        Random random = new Random();
        mId = random.nextInt();
        final NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_action_download)
                        .setContentTitle(getString(R.string.downloading))
                        .setContentText(item.artist + " - " + item.title);
// Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, MainActivity.class);

// The stack builder object will contain an artificial back stack for the
// started Activity.
// This ensures that navigating backward from the Activity leads out of
// your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
// Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
// Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        final NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.
//        mNotificationManager.notify(mId, mBuilder.build());


        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        // Do the "lengthy" operation 20 times
                        L.debug(fileLength);
                        while(true) {
                            L.debug(total);

                            if (total < fileLength && fileLength >0) {
                                int incr = total * 100 / fileLength;
//                        for (incr = 0; incr <= 100; incr+=5) {
                                // Sets the progress indicator to a max value, the
                                // current completion percentage, and "determinate"
                                // state
                                mBuilder.setProgress(100, incr, false);
                                // Displays the progress bar for the first time.
                                mNotificationManager.notify(mId, mBuilder.build());
                            } else {
//                        }
                                // When the loop is finished, updates the notification
                                mBuilder.setContentTitle(getString(R.string.downloading_complete))
                                        // Removes the progress bar
                                        .setProgress(0, 0, false);
                                mNotificationManager.notify(mId, mBuilder.build());
                                break;
                            }
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
// Starts the thread by calling the run() method in its Runnable
        ).start();
    }
}
