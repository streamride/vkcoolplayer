package com.streamride.mymoodstream.network.impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import com.streamride.mymoodstream.db.DBContentProvider;
import com.streamride.mymoodstream.models.VkAlbum;
import com.streamride.mymoodstream.network.HttpUtil;
import com.streamride.mymoodstream.network.OnRequestListener;
import com.streamride.mymoodstream.network.Operation;
import com.streamride.mymoodstream.parsers.AlbumParser;
import com.streamride.mymoodstream.utils.InternetStatus;
import com.streamride.mymoodstream.utils.L;
import org.json.JSONException;

import java.io.IOException;
import java.util.List;

/**
 * Created by andreyzakharov on 10.04.14.
 */
public class AlbumOperation extends Operation {

    private final Context context;
    private AlbumResponse albumResponse;

    public AlbumOperation(Context context, String url, OnRequestListener onRequestListener) {
        super(url, onRequestListener);
        this.context = context;
    }

    @Override
    protected void start() {
        HttpUtil httpUtil = new HttpUtil();
        InternetStatus internetStatus = new InternetStatus(context);
        try {
            final List<VkAlbum> firstFromDb = getDataFromDB(context);
            postInMainQueue(new Runnable() {
                @Override
                public void run() {
                    onRequestListener.onDbComplete(firstFromDb);
                }
            });
            if(internetStatus.isOnline()) {
                final String response = httpUtil.get(mUrl);
                parse(response);
                saveDataToDB();
                final List<VkAlbum> audioList = getDataFromDB(context);
                postInMainQueue(new Runnable() {
                    @Override
                    public void run() {
                        onRequestListener.onNetworkComplete(audioList);
                    }
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void parse(String response) {
        AlbumParser parser = new AlbumParser();
        try {
            albumResponse = parser.parse(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void saveDataToDB() {
        int result = 0;
        if(albumResponse.getResultList() != null){
            ContentValues[] contentValues = VkAlbum.getContentValues(albumResponse.getResultList());
            Log.d("", contentValues.toString());
            result = context.getContentResolver().bulkInsert(DBContentProvider.CONTENT_URI_ALBUM, contentValues);
            L.debug(result);
        }

        if(result == 0){
            onRequestListener.onError();
        }
    }

    private List<VkAlbum> getDataFromDB(Context context){
        String where = "";
        Cursor cursor = context.getContentResolver().query(DBContentProvider.CONTENT_URI_ALBUM,null,null,null,null);
        AlbumResponse albumResponse = new AlbumResponse();
        albumResponse.setResultList(VkAlbum.initFromCursor(context, cursor));
        return albumResponse.getResultList();
    }
}
