package com.streamride.mymoodstream.network.impl;

import android.content.Context;
import com.streamride.mymoodstream.models.TrackInfo;
import com.streamride.mymoodstream.network.HttpUtil;
import com.streamride.mymoodstream.network.OnRequestListener;
import com.streamride.mymoodstream.network.Request;
import com.streamride.mymoodstream.utils.C;
import com.streamride.mymoodstream.utils.InternetStatus;
import com.streamride.mymoodstream.utils.L;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by andreyzakharov on 19.05.14.
 */
public class SongDetailRequest extends Request {


    private final String title;
    private final String artist;
    private int lyricsId = 0;
    private HttpUtil httpUtil;

    public SongDetailRequest(Context context, String artist, String title, int lyricsId, OnRequestListener onRequestListener) {
        super(context, onRequestListener);
        this.artist = artist;
        this.title = title;
        this.lyricsId = lyricsId;
    }


    @Override
    protected boolean dispatch() {
        return start();
    }

    @Override
    protected void deliverResult() {

    }

    private boolean start(){
        httpUtil = new HttpUtil();
        L.debug("start track info");
        InternetStatus internetStatus = new InternetStatus(getContext());
        if(internetStatus.isOnline()){
            try {
                TrackInfo trackInfo = getSongDetail();
                final List<TrackInfo> trackInfoList = new ArrayList<TrackInfo>();
                deliver(trackInfo);

                trackInfo = getSongLyrics(trackInfo);
                deliver(trackInfo);

                return true;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    private void deliver(TrackInfo trackInfo){
        final List<TrackInfo> trackInfoList = new ArrayList<TrackInfo>();
        if(trackInfo != null) {
            L.debug(trackInfo);
            trackInfoList.add(trackInfo);
            postInMainQueue(new Runnable() {
                @Override
                public void run() {
                    mOnRequestListener.onNetworkComplete(trackInfoList);
                }
            });
        }
        else
            postInMainQueue(new Runnable() {
                @Override
                public void run() {
                    mOnRequestListener.onError();
                }
            });
    }

    private TrackInfo getSongDetail() throws IOException, JSONException {
        String url = C.URL.apiLastFmTrackInfo(artist, title);
        String response = httpUtil.get(url);
        L.debug(response);
        TrackInfo trackInfo = TrackInfo.parseFromJson(new JSONObject(response));
        return trackInfo;
    }

    private TrackInfo getSongLyrics(TrackInfo t) throws IOException, JSONException {
        String response = httpUtil.get(C.URL.apiGetSongLyrics(lyricsId));
        TrackInfo trackInfo = TrackInfo.parseLyrics(t, new JSONObject((response)));
        return trackInfo;
    }



}
