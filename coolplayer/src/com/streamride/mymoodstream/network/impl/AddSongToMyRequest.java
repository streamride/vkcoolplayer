package com.streamride.mymoodstream.network.impl;

import android.content.Context;
import com.streamride.mymoodstream.models.VkAudio;
import com.streamride.mymoodstream.network.HttpUtil;
import com.streamride.mymoodstream.network.OnRequestListener;
import com.streamride.mymoodstream.network.Request;
import com.streamride.mymoodstream.utils.C;
import com.streamride.mymoodstream.utils.InternetStatus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by andreyzakharov on 31.05.14.
 */
public class AddSongToMyRequest extends Request {

    private final VkAudio item;

    public AddSongToMyRequest(Context context, VkAudio item, OnRequestListener onRequestListener) {
        super(context, onRequestListener);
        this.item = item;
    }

    @Override
    protected boolean dispatch() {
        add();
        return false;
    }

    @Override
    protected void deliverResult() {

    }

    private boolean add(){
        HttpUtil httpUtil = new HttpUtil();
        InternetStatus internetStatus = new InternetStatus(getContext());
        try {
            if(internetStatus.isOnline()) {
                final String response = httpUtil.get(C.URL.apiAddSongToMy(item.id, item.owner_id));
                final int id = parse(response);
                postInMainQueue(new Runnable() {
                    @Override
                    public void run() {
                        mOnRequestListener.onNetworkComplete(Arrays.asList(id));
                    }
                });
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
            error();
        } catch (JSONException e) {
            e.printStackTrace();
            error();
        }
        return false;
    }


    private void error(){
        postInMainQueue(new Runnable() {
            @Override
            public void run() {
                mOnRequestListener.onError();
            }
        });
    }
    private int parse(String response) throws JSONException {
        JSONObject jsonObject = new JSONObject(response);
        int id = 0;
        if(jsonObject.has("response")){
            id = jsonObject.getInt("response");
        }
        return id;
    }
}
