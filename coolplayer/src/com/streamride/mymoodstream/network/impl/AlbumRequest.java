package com.streamride.mymoodstream.network.impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import com.streamride.mymoodstream.db.DBContentProvider;
import com.streamride.mymoodstream.models.VkAlbum;
import com.streamride.mymoodstream.network.HttpUtil;
import com.streamride.mymoodstream.network.OnRequestListener;
import com.streamride.mymoodstream.network.Request;
import com.streamride.mymoodstream.parsers.AlbumParser;
import com.streamride.mymoodstream.utils.InternetStatus;
import com.streamride.mymoodstream.utils.L;
import org.json.JSONException;

import java.io.IOException;
import java.util.List;

/**
 * Created by andreyzakharov on 08.05.14.
 */
public class AlbumRequest extends Request{


    private final String url;
    private AlbumResponse albumResponse;

    public AlbumRequest(Context context, String url, OnRequestListener onRequestListener) {
        super(context, onRequestListener);
        this.url = url;
    }

    @Override
    protected boolean dispatch() {
        return start();
    }

    @Override
    protected void deliverResult() {

    }

    protected boolean start() {
        HttpUtil httpUtil = new HttpUtil();
        InternetStatus internetStatus = new InternetStatus(getContext());
        try {
            final List<VkAlbum> firstFromDb = getDataFromDB(getContext());
            postInMainQueue(new Runnable() {
                @Override
                public void run() {
                    mOnRequestListener.onDbComplete(firstFromDb);
                }
            });
            if(internetStatus.isOnline()) {
                final String response = httpUtil.get(url);
                parse(response);
                saveDataToDB();
                final List<VkAlbum> audioList = getDataFromDB(getContext());
                postInMainQueue(new Runnable() {
                    @Override
                    public void run() {
                        mOnRequestListener.onNetworkComplete(audioList);
                    }
                });
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    protected void parse(String response) {
        AlbumParser parser = new AlbumParser();
        try {
            albumResponse = parser.parse(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void saveDataToDB() {
        int result = 0;
        if(albumResponse.getResultList() != null){
            ContentValues[] contentValues = VkAlbum.getContentValues(albumResponse.getResultList());
            Log.d("", contentValues.toString());
            result = getContext().getContentResolver().bulkInsert(DBContentProvider.CONTENT_URI_ALBUM, contentValues);
            L.debug(result);
        }

        if(result == 0){
            mOnRequestListener.onError();
        }
    }

    private List<VkAlbum> getDataFromDB(Context context){
        String where = "";
        Cursor cursor = context.getContentResolver().query(DBContentProvider.CONTENT_URI_ALBUM,null,null,null,null);
        AlbumResponse albumResponse = new AlbumResponse();
        albumResponse.setResultList(VkAlbum.initFromCursor(context, cursor));
        return albumResponse.getResultList();
    }
}
