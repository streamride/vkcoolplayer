package com.streamride.mymoodstream.network.impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import com.streamride.mymoodstream.db.DBContentProvider;
import com.streamride.mymoodstream.models.VkAudio;
import com.streamride.mymoodstream.network.HttpUtil;
import com.streamride.mymoodstream.network.OnRequestListener;
import com.streamride.mymoodstream.network.Request;
import com.streamride.mymoodstream.parsers.MusicListParser;
import com.streamride.mymoodstream.utils.C;
import com.streamride.mymoodstream.utils.InternetStatus;
import com.streamride.mymoodstream.utils.L;
import com.streamride.mymoodstream.utils.QueryBuilder;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by andreyzakharov on 08.05.14.
 */
public class AudioRequest extends Request {


    private final String playlist;
    private final int albumId;
    private final String url;
    private AudioResponse audioResponse;

    public AudioRequest(Context context, String url, String playList, int album_id, OnRequestListener onRequestListener) {
        super(context, onRequestListener);
        this.playlist = playList;
        this.albumId = album_id;
        this.url = url;

    }

    @Override
    protected boolean dispatch() {
        return start();
    }

    @Override
    protected void deliverResult() {

    }


    protected boolean start() {
        HttpUtil httpUtil = new HttpUtil();
        InternetStatus internetStatus = new InternetStatus(getContext());
        try {
            final List<VkAudio> firstFromDb = getDataFromDB(getContext(), playlist);
            postInMainQueue(new Runnable() {
                @Override
                public void run() {
                    mOnRequestListener.onDbComplete(firstFromDb);
                }
            });
            if(C.PLAYLIST.SAVED.equals(playlist))
                return true;
            if(internetStatus.isOnline()) {
                final String response = httpUtil.get(url);
                parse(response);
                deleteDataFromDB();
                saveDataToDB();
                final List<VkAudio> audioList = getDataFromDB(getContext(), playlist);
                postInMainQueue(new Runnable() {
                    @Override
                    public void run() {
                        mOnRequestListener.onNetworkComplete(audioList);
                    }
                });
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    protected void parse(String response) {
        MusicListParser parser = new MusicListParser();
        try {
            audioResponse = parser.parse(response, playlist);
            L.debug(audioResponse);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void deleteDataFromDB(){
        String where = VkAudio.PLAYLIST + " = " + "'" + playlist + "'";
        try {
            getContext().getContentResolver().delete(DBContentProvider.CONTENT_URI_AUDIO, where, null);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    protected void saveDataToDB() {
        int result = 0;
        if(audioResponse.getResultList() != null){
            ContentValues[] contentValues = VkAudio.getContentValues(audioResponse.getResultList());
            Log.d("", contentValues.toString());
            result = getContext().getContentResolver().bulkInsert(DBContentProvider.CONTENT_URI_AUDIO, contentValues);
            L.debug(result);
        }

        if(result == 0){
            mOnRequestListener.onError();
        }
    }

    private List<VkAudio> getDataFromDB(Context context, String playlist){
        if(C.PLAYLIST.SAVED.equals(playlist))
            return getSavedAudios();
        String where = VkAudio.PLAYLIST + " = " + "'" + playlist + "'";
        if(C.PLAYLIST.ALBUM_ID.equals(playlist))
            where += " AND " + VkAudio.ALBUM_ID + " = " + albumId;
        Cursor cursor = context.getContentResolver().query(DBContentProvider.CONTENT_URI_AUDIO,null,where,null,null);
        AudioResponse audioResponse = new AudioResponse();
        List<VkAudio> vkAudioList = VkAudio.initFromCursor(context, cursor);
        getSavedAudios(vkAudioList);
        audioResponse.setResultList(vkAudioList);
        cursor.close();
        L.debug(audioResponse);
        return audioResponse.getResultList();
    }

    private List<VkAudio> getSavedAudios(){
        QueryBuilder queryBuilder = new QueryBuilder();
        Cursor cursor = queryBuilder.query(getContext(), DBContentProvider.CONTENT_URI_SAVED);
        AudioResponse audioResponse = new AudioResponse();
        List<VkAudio> vkAudioList = VkAudio.initFromCursor(getContext(), cursor);
        audioResponse.setResultList(vkAudioList);
        return audioResponse.getResultList();
    }

    private void getSavedAudios(List<VkAudio> vkAudios){
        if(vkAudios != null && vkAudios.size() > 0) {
            Cursor cursor = getContext().getContentResolver().query(DBContentProvider.CONTENT_URI_SAVED, null, null, null, null);
            List<Integer> savedList = new ArrayList<Integer>();
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    int id = cursor.getInt(cursor.getColumnIndex(VkAudio.ID));
                    savedList.add(id);
                } while (cursor.moveToNext());
            }

            for(int i=0;i<vkAudios.size();i++){
                for(int j=0; j< savedList.size();j++) {
                    if (vkAudios.get(i).id == savedList.get(j)){
                        vkAudios.get(i).isSaved = true;
                    }
                }
            }
        }
    }
}
