package com.streamride.mymoodstream.network.impl;

import android.content.Context;
import com.streamride.mymoodstream.network.HttpUtil;
import com.streamride.mymoodstream.network.OnRequestListener;
import com.streamride.mymoodstream.network.Request;
import com.streamride.mymoodstream.parsers.MusicListParser;
import com.streamride.mymoodstream.utils.C;
import com.streamride.mymoodstream.utils.InternetStatus;
import com.streamride.mymoodstream.utils.L;
import org.json.JSONException;

import java.io.IOException;

/**
 * Created by andreyzakharov on 13.05.14.
 */
public class SearchVkRequest extends Request {

    private final String searchStr;
    private AudioResponse audioResponse;

    public SearchVkRequest(Context context, String searchStr, OnRequestListener onRequestListener) {
        super(context, onRequestListener);
        this.searchStr = searchStr;
    }

    @Override
    protected boolean dispatch() {
        HttpUtil httpUtil = new HttpUtil();
        InternetStatus internetStatus = new InternetStatus(getContext());
        if(internetStatus.isOnline()){
            try {
                final String response = httpUtil.get(C.URL.apiSearchAudio(searchStr));
                parse(response);
                postInMainQueue(new Runnable() {
                    @Override
                    public void run() {
                        mOnRequestListener.onNetworkComplete(audioResponse.getResultList());
                    }
                });
            }
            catch(IOException e){
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    protected void deliverResult() {

    }

    protected void parse(String response) {
        MusicListParser parser = new MusicListParser();
        try {
            audioResponse = parser.parse(response,"search");
            L.debug(audioResponse);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
