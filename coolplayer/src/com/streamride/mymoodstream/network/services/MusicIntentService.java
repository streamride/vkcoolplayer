package com.streamride.mymoodstream.network.services;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;
import com.streamride.mymoodstream.db.DBContentProvider;
import com.streamride.mymoodstream.models.VkAudio;
import com.streamride.mymoodstream.network.HttpUtil;
import com.streamride.mymoodstream.network.impl.AudioResponse;
import com.streamride.mymoodstream.parsers.MusicListParser;
import com.streamride.mymoodstream.utils.InternetStatus;
import com.streamride.mymoodstream.utils.L;
import org.json.JSONException;

import java.io.IOException;

/**
 * Created by andreyzakharov on 21.07.14.
 */
public class MusicIntentService extends IntentService {
    private InternetStatus internetStatus;
    private String playlist;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public MusicIntentService() {
        super("musicintentservice");
    }


    @Override
    public void onCreate() {
        super.onCreate();
        internetStatus = new InternetStatus(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        ResultReceiver rr = intent.getParcelableExtra("receiver");
        if(internetStatus.isOnline()) {
            String url = intent.getStringExtra("url");
            playlist = intent.getStringExtra("playlist");
            HttpUtil httpUtil = new HttpUtil();
            final String response;
            try {
                response = httpUtil.get(url);
                AudioResponse audioResponse = parse(response);
                deleteDataFromDB();
                saveDataToDB(audioResponse);
                Bundle bundle = new Bundle();
                bundle.putInt("status",1);
                rr.send(1, bundle);
            } catch (IOException e) {
                e.printStackTrace();
                rr.send(0, null);
            } catch (NullPointerException e){
                e.printStackTrace();
                rr.send(0, null);
            }
        }
        else{
            rr.send(0, null);
        }
    }

    protected AudioResponse parse(String response) {
        MusicListParser parser = new MusicListParser();
        try {
            AudioResponse audioResponse = parser.parse(response, playlist);
            return audioResponse;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void deleteDataFromDB(){
        String where = VkAudio.PLAYLIST + " = " + "'" + playlist + "'";
        try {
            getContentResolver().delete(DBContentProvider.CONTENT_URI_AUDIO, where, null);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    protected void saveDataToDB(AudioResponse audioResponse) {
        int result = 0;
        if(audioResponse.getResultList() != null){
            ContentValues[] contentValues = VkAudio.getContentValues(audioResponse.getResultList());
            Log.d("", contentValues.toString());
            result = getContentResolver().bulkInsert(DBContentProvider.CONTENT_URI_AUDIO, contentValues);
            L.debug(result);
        }
    }
}
