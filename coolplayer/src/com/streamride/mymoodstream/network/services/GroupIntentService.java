package com.streamride.mymoodstream.network.services;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;
import com.streamride.mymoodstream.db.DBContentProvider;
import com.streamride.mymoodstream.models.VkAudio;
import com.streamride.mymoodstream.models.VkGroup;
import com.streamride.mymoodstream.parsers.GroupParser;
import com.streamride.mymoodstream.utils.C;
import com.streamride.mymoodstream.utils.L;
import org.json.JSONException;

import java.io.IOException;
import java.util.List;

/**
 * Created by andreyzakharov on 31.07.14.
 */
public class GroupIntentService extends BaseIntentService {


    @Override
    protected void handleIntent(ResultReceiver rr, Intent intent) {


        try {
            String response = httpUtil.get(C.URL.apiGetGroupList());
            GroupParser parser = new GroupParser();
            List<VkGroup> groupList = parser.parse(response);
            deleteDataFromDB();
            saveDataToDB(groupList);
            sendOk(rr);
        } catch (IOException e) {
            e.printStackTrace();
            rr.send(0, null);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    private void deleteDataFromDB(){

        try {
            getContentResolver().delete(DBContentProvider.CONTENT_URI_GROUP, null, null);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    protected void saveDataToDB(List<VkGroup> vkGroupList) {
        int result = 0;
        if(vkGroupList != null){
            ContentValues[] contentValues = VkGroup.getContentValues(vkGroupList);
            Log.d("", contentValues.toString());
            result = getContentResolver().bulkInsert(DBContentProvider.CONTENT_URI_GROUP, contentValues);
            L.debug(result);
        }
    }
}
