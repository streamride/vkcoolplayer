package com.streamride.mymoodstream.network.services;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.os.ResultReceiver;
import android.util.Log;
import com.streamride.mymoodstream.db.DBContentProvider;
import com.streamride.mymoodstream.models.VkAlbum;
import com.streamride.mymoodstream.network.HttpUtil;
import com.streamride.mymoodstream.network.impl.AlbumResponse;
import com.streamride.mymoodstream.parsers.AlbumParser;
import com.streamride.mymoodstream.utils.InternetStatus;
import com.streamride.mymoodstream.utils.L;
import org.json.JSONException;

import java.io.IOException;

/**
 * Created by andreyzakharov on 21.07.14.
 */
public class AlbumIntentService extends IntentService{
    private InternetStatus internetStatus;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public AlbumIntentService() {
        super("albums");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        internetStatus = new InternetStatus(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String url = intent.getStringExtra("url");
        ResultReceiver resultReceiver = intent.getParcelableExtra("receiver");
        HttpUtil httpUtil = new HttpUtil();
        if(internetStatus.isOnline()) {
            final String response;
            try {
                response = httpUtil.get(url);
                AlbumResponse albumResponse = parse(response);
                saveDataToDB(albumResponse);
                resultReceiver.send(1, null);
            } catch (IOException e) {

            }
        }
        else{
            resultReceiver.send(0, null);
        }

    }

    protected AlbumResponse parse(String response) {
        AlbumParser parser = new AlbumParser();
        try {
            AlbumResponse albumResponse = parser.parse(response);
            return albumResponse;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void saveDataToDB(AlbumResponse albumResponse) {
        int result = 0;
        if(albumResponse.getResultList() != null){
            ContentValues[] contentValues = VkAlbum.getContentValues(albumResponse.getResultList());
            Log.d("", contentValues.toString());
            result = getContentResolver().bulkInsert(DBContentProvider.CONTENT_URI_ALBUM, contentValues);
            L.debug(result);
        }
    }
}
