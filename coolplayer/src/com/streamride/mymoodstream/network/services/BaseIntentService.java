package com.streamride.mymoodstream.network.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import com.streamride.mymoodstream.network.HttpUtil;
import com.streamride.mymoodstream.utils.InternetStatus;
import com.streamride.mymoodstream.utils.L;

/**
 * Created by andreyzakharov on 31.07.14.
 */
public abstract class BaseIntentService extends IntentService{

    protected InternetStatus internetStatus;
    protected HttpUtil httpUtil;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public BaseIntentService() {
        super(BaseIntentService.class.getName());
    }


    @Override
    public void onCreate() {
        super.onCreate();
        internetStatus = new InternetStatus(this);
        httpUtil = new HttpUtil();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        L.debug("on handle intent ");
        ResultReceiver rr = intent.getParcelableExtra("receiver");
        if(internetStatus.isOnline()) {
            L.debug(" online ");
            handleIntent(rr, intent);
        }
        else{
            sendError(rr);
        }
    }

    protected void sendOk(ResultReceiver rr){
        Bundle bundle = new Bundle();
        bundle.putInt("status",1);
        rr.send(1, bundle);
    }

    protected void sendError(ResultReceiver rr){
        rr.send(0, null);
    }



    protected abstract void handleIntent(ResultReceiver rr, Intent intent);
}
