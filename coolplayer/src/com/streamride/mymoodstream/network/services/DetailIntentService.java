package com.streamride.mymoodstream.network.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import com.streamride.mymoodstream.models.TrackInfo;
import com.streamride.mymoodstream.network.HttpUtil;
import com.streamride.mymoodstream.utils.C;
import com.streamride.mymoodstream.utils.InternetStatus;
import com.streamride.mymoodstream.utils.L;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by andreyzakharov on 21.07.14.
 */
public class DetailIntentService extends IntentService{

    private HttpUtil httpUtil;
    private int lyricsId;
    private String title;
    private String artist;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public DetailIntentService() {
        super("detail");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        httpUtil = new HttpUtil();
        ResultReceiver resultReceiver = intent.getParcelableExtra("receiver");
        artist = intent.getStringExtra("artist");
        title = intent.getStringExtra("title");
        lyricsId = intent.getIntExtra("lyricsId",0);
        InternetStatus internetStatus = new InternetStatus(this);
        if(internetStatus.isOnline()){
            try {
                TrackInfo trackInfo = getSongDetail();

                trackInfo = getSongLyrics(trackInfo);
                Bundle bundle = new Bundle();
                bundle.putSerializable("trackinfo", trackInfo);
                resultReceiver.send(1, bundle);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                resultReceiver.send(0,null);
            } catch (IOException e) {
                e.printStackTrace();
                resultReceiver.send(0,null);
            } catch (JSONException e) {
                e.printStackTrace();
                resultReceiver.send(0,null);
            }
        }
        else{
            resultReceiver.send(0,null);
        }

    }




    private TrackInfo getSongDetail() throws IOException, JSONException {
        String url = C.URL.apiLastFmTrackInfo(artist, title);
        String response = httpUtil.get(url);
        L.debug(response);
        TrackInfo trackInfo = TrackInfo.parseFromJson(new JSONObject(response));
        return trackInfo;
    }

    private TrackInfo getSongLyrics(TrackInfo t) throws IOException, JSONException {
        String response = httpUtil.get(C.URL.apiGetSongLyrics(lyricsId));
        TrackInfo trackInfo = TrackInfo.parseLyrics(t, new JSONObject((response)));
        return trackInfo;
    }
}
