package com.streamride.mymoodstream.network;

import java.util.List;

/**
 * Created by andreyzakharov on 05.04.14.
 */
public abstract class Response<T> {

    protected List<T> resultList;

    public void setResultList(List<T> resultList){
        this.resultList = resultList;
    }

    public List<T> getResultList(){
        return this.resultList;
    }
}
