package com.streamride.mymoodstream.network;

import android.app.Service;
import android.content.Intent;
import android.os.*;
import android.os.Process;
import android.support.v4.content.LocalBroadcastManager;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created by andreyzakharov on 07.05.14.
 */
public class HttpService extends Service {

    private static final Executor mBackgroundExecutor = Executors.newCachedThreadPool();
    private static final Queue<Request> mRequestEnqueue = new LinkedList<Request>();
    public static final String RESULT = "result";

    private HttpServiceHandler mServiceHandler;
    private Looper mServiceLooper;


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    public static void enqueueRequest(Request request){
        mRequestEnqueue.add(request);

        Intent httpService = new Intent(request.getContext(), HttpService.class);

        request.getContext().startService(httpService);
    }



    private final class HttpServiceHandler extends Handler {
        public HttpServiceHandler(Looper looper){
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
//            super.handleMessage(msg);

            if(!mRequestEnqueue.isEmpty()){
                Request request = mRequestEnqueue.poll();
                boolean result = request.dispatch();
                sendBroadcast(request.getBroadcastName(),result);
            }
        }
    }


    @Override
    public void onCreate() {
        super.onCreate();
        HandlerThread thread = new HandlerThread("HttpService", Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        mServiceLooper = thread.getLooper();
        mServiceHandler = new HttpServiceHandler(mServiceLooper);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);
        return START_STICKY;
    }


    private void sendBroadcast(String name, boolean result){
        Intent intent = new Intent(name);
        intent.putExtra(RESULT, result);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}
