package com.streamride.mymoodstream.network;

import java.util.List;

/**
 * Created by andreyzakharov on 03.04.14.
 */
public interface OnRequestListener<T> {

    public void onNetworkComplete(List<T> result);
    public void onError();
    public void OnProgress(int progress);
    public void onDbComplete(List<T> result);
}
