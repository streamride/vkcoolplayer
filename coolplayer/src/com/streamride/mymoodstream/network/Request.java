package com.streamride.mymoodstream.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import com.streamride.mymoodstream.utils.L;

/**
 * Created by andreyzakharov on 03.04.14.
 */
public abstract class Request <T>{


    private static Handler mMainThreadHandler;
    protected OnRequestListener mOnRequestListener;
    private String mBroadcastName;
    private Context mContext;


    public Request(Context context, OnRequestListener<T> onRequestListener){
        mContext = context;
        mOnRequestListener = onRequestListener;
    }

    public Context getContext(){
        return mContext;
    }


    public String getBroadcastName(){
        return mBroadcastName;
    }

    protected static Handler getMainThreadHandler() {
        if (mMainThreadHandler == null) {
            mMainThreadHandler = new Handler(Looper.getMainLooper());
        }
        return mMainThreadHandler;
    }

    public static void postInMainQueue(Runnable r) {
        getMainThreadHandler().post(r);
    }



    protected abstract boolean dispatch();
    protected abstract void deliverResult();



    public void destroy(){
        if(mContext != null)
            try {
                mContext.unregisterReceiver(mBroadcastReceiver);
            }
            catch (IllegalArgumentException e){
                L.debug(e.toString());
            }
    }




    public void execute(){

        IntentFilter filter = new IntentFilter();
        filter.addAction("RESULT");
        mContext.registerReceiver(mBroadcastReceiver, filter);
        mBroadcastName = "dddd";
        HttpService.enqueueRequest(this);

    }




    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            destroy();
            if(intent != null && intent.getBooleanExtra(HttpService.RESULT, false)){
                deliverResult();
            }
        }
    };


}
