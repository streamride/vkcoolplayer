package com.streamride.mymoodstream.network;

import android.os.Handler;
import android.os.Looper;

/**
 * Created by andreyzakharov on 05.04.14.
 */
public abstract class Operation {

    private static Handler mMainThreadHandler;
    protected final OnRequestListener onRequestListener;
    protected String mUrl;

    public Operation(String url, OnRequestListener onRequestListener){
        mUrl = url;
        this.onRequestListener = onRequestListener;
    }

    protected abstract void start();

    protected abstract void parse(String response);

    protected abstract void saveDataToDB();


    protected static Handler getMainThreadHandler() {
        if (mMainThreadHandler == null) {
            mMainThreadHandler = new Handler(Looper.getMainLooper());
        }
        return mMainThreadHandler;
    }

    public static void postInMainQueue(Runnable r) {
        getMainThreadHandler().post(r);
    }

}
