package com.streamride.mymoodstream.network;

import android.os.Looper;
import com.squareup.okhttp.OkHttpClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created by andreyzakharov on 03.04.14.
 */
public class HttpUtil {


    private static final Executor mBackgroundExecutor = Executors.newCachedThreadPool();

    public String get(String urlStr) throws IOException{
       return new String(getBytes(urlStr),"UTF-8");
    }

    public byte[] getBytes(String urlStr) throws IOException{
        OkHttpClient okHttpClient = new OkHttpClient();
        URL url = null;
        try {
            url = new URL(urlStr);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection connection = okHttpClient.open(url);
        InputStream in = null;
        try {
            in = connection.getInputStream();
            byte[] response = readStream(in);
            return response;
        }
        finally {
            if(in != null)
                in.close();
        }
    }


    private byte[] readStream(InputStream in) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        for (int count; (count = in.read(buffer)) != -1; ) {
            out.write(buffer, 0, count);
        }
        return out.toByteArray();
    }


    public static void enqueueOperation(Operation operation){
        operation.start();
    }


    public static void enqueueAsyncOperation(final Operation operation) {
        //Check thread
        if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
            mBackgroundExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    enqueueAsyncOperation(operation);
                }
            });
            return;
        }

        operation.start();
    }


}
