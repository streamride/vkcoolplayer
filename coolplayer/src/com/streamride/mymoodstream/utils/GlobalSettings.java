package com.streamride.mymoodstream.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by andreyzakharov on 27.05.14.
 */
public enum  GlobalSettings {

    GLOBAL_SETTINGS;


    public void setIsChangeSongByShake(Context context, boolean isShake){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("isshake", isShake);
        editor.commit();
    }

    public boolean isChangeSongByShake(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean("isshake", false);
    }
}
