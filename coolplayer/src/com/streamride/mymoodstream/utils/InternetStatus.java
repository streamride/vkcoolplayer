package com.streamride.mymoodstream.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by andreyzakharov on 04.04.14.
 */
public class InternetStatus {

    private final ConnectivityManager connectivity;
    private NetworkInfo  networkInfo;

    public InternetStatus(Context context) {
        connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public int getNetwork() {
        networkInfo = connectivity.getActiveNetworkInfo();
        return networkInfo.getType();
    }

    public boolean isOnline() {
        final NetworkInfo ni = connectivity.getActiveNetworkInfo();
        if (ni != null && ni.isAvailable() && ni.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

}
