package com.streamride.mymoodstream.utils;

/**
 * Created by andreyzakharov on 07.04.14.
 */
public class DateUtils {

    public static String secondsToMinutes(int s){
        int minutes = s/60;
        int seconds = s%60;
        String secondsStr = String.valueOf(seconds);
        if(seconds < 10)
            secondsStr = "0" + String.valueOf(seconds);
        StringBuilder stringBuilder = new StringBuilder(1024);
        stringBuilder.append(minutes).append(":").append(secondsStr);
        return stringBuilder.toString();
    }
}
