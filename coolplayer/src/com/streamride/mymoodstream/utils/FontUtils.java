package com.streamride.mymoodstream.utils;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by andreyzakharov on 07.04.14.
 */
public class FontUtils {

    public static Typeface getArtistFont(Context c){
        return Typeface.createFromAsset(c.getAssets(), "OPENSANS-LIGHT.TTF");
    }

    public static Typeface getSongNameFont(Context c){
        return Typeface.createFromAsset(c.getAssets(), "OPENSANS-REGULAR.TTF");
    }

}
