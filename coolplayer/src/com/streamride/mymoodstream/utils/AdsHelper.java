package com.streamride.mymoodstream.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by andreyzakharov on 21.07.14.
 */
public enum AdsHelper {

    INSTANCE;

    public void setShowAds(Context context, boolean isShow){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("isshow", isShow);
        editor.commit();
    }

    public boolean isShowAds(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean("isshow", true);
    }
}
