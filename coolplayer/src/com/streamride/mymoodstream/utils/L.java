package com.streamride.mymoodstream.utils;

import android.util.Log;

/**
 * Created by andreyzakharov on 03.04.14.
 */
public class L {

    private static boolean DEBUG = true;
    public static void debug(Object object){
        if(DEBUG)
            Log.d("", object.toString());
    }
}
