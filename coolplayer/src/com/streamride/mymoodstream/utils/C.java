package com.streamride.mymoodstream.utils;

import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApiConst;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by andreyzakharov on 03.04.14.
 */
public class C {

    public static class VK {
        public static final String APP_ID = "4284143";
        public static final String[] sMyScope = new String[] {
                VKScope.AUDIO,
                VKScope.OFFLINE
        };
    }

    public static class URL {
        public static final String VK_API = "https://api.vk.com/method/";
        public static final String GET_MUSIC = "audio.get";
        public static final String GET_RECOMMENDATIONS = "audio.getRecommendations";
        public static final String GET_POPULAR = "audio.getPopular";
        public static final String GET_ALBUMS = "audio.getAlbums";
        public static final String SEARCH_AUDIOS = "audio.search";
        public static final String GET_SONG_LYRICS = "audio.getLyrics";
        public static final String ADD_SONG_TO_MY = "audio.add";
        public static final String GET_GROUPS_URL = "groups.get";

        public static final String LASTFM_TRACK_INFO = "http://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key=";
        private static final String LASTFM_API_KEY = "62b0949d236f1c945782953b240fb02b";


        public static String apiGetGroupList(){
            return VK_API + GET_GROUPS_URL + "?user_id=" + VKSdk.getAccessToken().userId + "&extended=1" + "&v=5.23&" + VKApiConst.ACCESS_TOKEN + "=" + VKSdk.getAccessToken().accessToken;
        }


        public static String apiLastFmTrackInfo(String artist, String title) throws UnsupportedEncodingException {
            return LASTFM_TRACK_INFO + LASTFM_API_KEY + "&artist=" + URLEncoder.encode(artist, "utf-8") + "&track=" + URLEncoder.encode(title, "utf-8") + "&format=json";
        }

        public static final String apiAddSongToMy(int audioId, int ownerId){
            return VK_API + ADD_SONG_TO_MY + "?audio_id=" + audioId + "&" + VKApiConst.OWNER_ID + "=" + ownerId
            + "&" + VKApiConst.VERSION + "=5.17&" + VKApiConst.ACCESS_TOKEN + "=" + VKSdk.getAccessToken().accessToken;
        }

        public static final String apiGetSongLyrics(int lyricsId){
            return VK_API + GET_SONG_LYRICS + "?lyrics_id=" + lyricsId + "&" + VKApiConst.VERSION + "=5.17&" +
                    VKApiConst.ACCESS_TOKEN + "=" +VKSdk.getAccessToken().accessToken;
        }


        public static String apiMyMusicUrl(){
            if(VKSdk.getAccessToken() != null) {
                return VK_API + GET_MUSIC + "?" + VKApiConst.OWNER_ID + "=" + VKSdk.getAccessToken().userId + "&" + VKApiConst.VERSION + "=5.17&" +
                        VKApiConst.ACCESS_TOKEN + "=" + VKSdk.getAccessToken().accessToken;
            }
            return null;
        }

        public static String apiMusicFromAlbumUrl(int albumId){
            if(VKSdk.getAccessToken() != null) {
                return VK_API + GET_MUSIC + "?" + VKApiConst.ALBUM_ID + "=" + albumId + "&" + VKApiConst.VERSION + "=5.17&" +
                        VKApiConst.ACCESS_TOKEN + "=" + VKSdk.getAccessToken().accessToken;
            }
            return null;
        }

        public static String apiRecommendationUrl(){
            return VK_API + GET_RECOMMENDATIONS + "?" + VKApiConst.USER_ID + "=" + VKSdk.getAccessToken().userId + "&" + VKApiConst.VERSION + "=5.17&"
                    + VKApiConst.ACCESS_TOKEN + "=" + VKSdk.getAccessToken().accessToken;
        }

        public static String apiPopularUrl(){
            return VK_API + GET_POPULAR + "?" + VKApiConst.VERSION + "=5.17&"
                    + VKApiConst.ACCESS_TOKEN + "=" + VKSdk.getAccessToken().accessToken;
        }

        public static String apiMyAlbumsUrl(){
            if(VKSdk.getAccessToken() != null) {
                return VK_API + GET_ALBUMS + "?" + VKApiConst.OWNER_ID + "=" + VKSdk.getAccessToken().userId + "&" + VKApiConst.VERSION + "=5.17&" +
                        VKApiConst.ACCESS_TOKEN + "=" + VKSdk.getAccessToken().accessToken;
            }
            return null;
        }

        public static String apiSearchAudio(String str){
            return VK_API + SEARCH_AUDIOS + "?" + VKApiConst.Q + "=" + str + "&" + "auto_complete=1" + "&" + VKApiConst.VERSION + "=5.17&" +
                    VKApiConst.ACCESS_TOKEN + "=" + VKSdk.getAccessToken().accessToken;
        }
    }

    public static class DB {
        public static final String DATABASE_NAME = "coolplayer.db";
        public static final int DATABASE_VERSION = 5;
    }

    public static class PLAYER {
        public static final String AUDIO_ID = "id";
        public static final String AUDIO_ID_ARRAY = "id_array";
        public static final String POSITION = "position";
        public static final String BUFFER = "buffer";
        public static final String CURRENT_TIME = "current_time";
        public static final String STATE = "state";
    }

    public static class ACTIONS {
        public static final String BUFFER = "buffer";
        public static final String NEW_SONG = "new_song";
        public static final String CURRENT_POSITION = "current_position";
        public static final String PLAYER_STATE = "player_state";
        public static final String SONG_DOWNLOADED = "song_downloaded";
    }

    public static class PLAYER_STATES {
        public static final int PLAYING = 1;
        public static final int PAUSED = 2;
        public static final int STOPPED = 0;
        public static final int PREPARING = 4;
    }

    public static class PLAYLIST {
        public static final String MY_MUSIC = "my_music";
        public static final String RECOMMEND = "recommend";
        public static final String ALBUM_ID = "album_id";
        public static final String SAVED = "saved";
    }

    public static final String test_url = "http://www.adver.mobi/cgi-bin/b.pl?siteid=307&m=5";

public static final String ADS =

        "<!DOCTYPE HTML> " +
                "<html xmlns=\"http://www.w3.org/1999/xhtml\" >" +

                "<head></head> <body style=\"margin:0 0 0 0; padding:0 0 0 0;\"> " +
                "<iframe width=\"320px\" height=\"50px\" src=\"http://www.adver.mobi/cgi-bin/b.pl?siteid=307&m=5\" frameborder=\"0\" framespacing=\"0\" border=\"0\" scrolling=\"no\"></iframe> " +
                "</body> </html>";

    public static class LOAD {
        public static final String IS_NETWORK_NEED = "is_network_need";
    }

    public final static String BILLING_BASE64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAun4W2ZODTSH4zPfuqaVkA3zo2DG9JQ5mUgoFJdlyyOi/mrjJym5IU9Ij1vAg731VpOZhd0WiKBkCtsrEY6vL/HNlGYND+jQX658w4mbdcU5i8RMSzUoDtI1Y1W2mHHnbnHWycOEQlX4nVN0gkk1WeDtQ83ydSic2suZZMGPu/O4n2fPFU/STlFs3PqP7mo3K490mi5+elqBD8RYbITBk2+paTse2rro88dYwAfbSJrt9UzQO8haSbas29a7Ygg6sGpVkQvNLXOcrrDXDBka2FmpXpqqC1nABzHfYfnVTm/enUdF2aIRPMhOvtZnd1lPL9KRTyxdB4o06gIpukGuSXwIDAQAB";
    public final static String DELETE_ADS = "delete_ads";

//        "<iframe width=\"320px\" height=\"50px\" src=\"http://www.adver.mobi/cgi-bin/b.pl?siteid=307&m=5\" frameborder=\"0\" scrolling=\"no\"></iframe> ";

}
