package com.streamride.mymoodstream.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.streamride.moodstream.R;
import com.streamride.mymoodstream.models.Playlist;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andreyzakharov on 13.05.14.
 */
public class PlaylistAdapter extends BaseAdapter {


    private final Context context;
    private final LayoutInflater mInflater;
    private List<Playlist> playlistList = new ArrayList<Playlist>();

    public PlaylistAdapter(Context context){
        this.context = context;
        mInflater = LayoutInflater.from(context);
    }

    public void setData(List<Playlist> playlistList){
        if(playlistList != null){
            this.playlistList = playlistList;
            notifyDataSetChanged();
        }
    }

    @Override
    public int getCount() {
        return playlistList.size();
    }

    @Override
    public Playlist getItem(int position) {
        return playlistList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if(convertView == null){
            viewHolder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.playlist_item,null);
            viewHolder.name = (TextView) convertView.findViewById(R.id.playlist_name);
            viewHolder.background = (ImageView) convertView.findViewById(R.id.background_img);
            viewHolder.checkPlaylist = (ImageView) convertView.findViewById(R.id.check_playlist);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Playlist item = getItem(position);

        viewHolder.name.setText(item.name);
        Picasso.with(context).load(item.img).into(viewHolder.background);




        return convertView;
    }

    static class ViewHolder {
        TextView name;
        ImageView background;
        ImageView checkPlaylist;
    }
}
