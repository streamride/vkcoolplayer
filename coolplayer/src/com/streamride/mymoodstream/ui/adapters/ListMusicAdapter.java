package com.streamride.mymoodstream.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.streamride.moodstream.R;
import com.streamride.mymoodstream.models.VkAudio;
import com.streamride.mymoodstream.network.OnRequestListener;
import com.streamride.mymoodstream.network.impl.AddSongToMyRequest;
import com.streamride.mymoodstream.network.impl.DownloadIntentService;
import com.streamride.mymoodstream.utils.DateUtils;
import com.streamride.mymoodstream.utils.FontUtils;
import com.vk.sdk.VKSdk;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andreyzakharov on 03.04.14.
 */
public class ListMusicAdapter extends BaseAdapter {

    private static final int TYPE_SONG = 0;
    private static final int TYPE_TITLE = 1;
    private final Context mContext;
    private final LayoutInflater mInflater;
    private List<VkAudio> audioList = new ArrayList<VkAudio>();
    private List<VkAudio> mainAudioList = new ArrayList<VkAudio>();
    private List<VkAudio> searchAudioList = new ArrayList<VkAudio>();
    private boolean isSearching = false;
//    private View.OnClickListener songClickListener;

    public ListMusicAdapter(Context context){
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
    }


    public void setMusicList(List<VkAudio> audioList){
        this.audioList = audioList;
        this.mainAudioList.addAll(audioList);
        notifyDataSetChanged();
    }

    

    public void setSearchData(List<VkAudio> audioList){
        this.audioList = audioList;
        notifyDataSetChanged();
    }

    public void setVkSearchData(List<VkAudio> audioList){
        if(audioList != null){
            this.searchAudioList = audioList;
            notifyDataSetChanged();
        }
    }

    public void clearSearch(){
        this.audioList.clear();
        setIsSearching(false);
        this.audioList = mainAudioList;
        notifyDataSetChanged();
    }
    
    public void setSongClickListener(View.OnClickListener songClickListener){
//        this.songClickListener = songClickListener;
    }

    public ArrayList<Integer> getAudioIds(){
        ArrayList<Integer> audio_ids = new ArrayList<Integer>();
        for(int i=0;i<this.audioList.size();i++){
            audio_ids.add(this.audioList.get(i).id);
        }

        return audio_ids;
    }

    public VkAudio getItemByAudioId(int audioId){
        for(int i=0;i<this.audioList.size();i++){
            if(this.audioList.get(i).id == audioId){
                return this.audioList.get(i);
            }
        }
        return null;
    }


    public void updateById(int audio_id){
        for(int i=0;i<this.audioList.size();i++){
            if(this.audioList.get(i).id == audio_id){
                setAllNotPlaying();
                this.audioList.get(i).setPlayingState(VkAudio.PLAYING_STATE.playing);
                break;
            }
        }
        notifyDataSetChanged();
    }

    private void setAllNotPlaying(){
        for(int i=0;i<this.audioList.size();i++){
            VkAudio vkAudio = this.audioList.get(i);
            if(vkAudio.getPlayingState() == VkAudio.PLAYING_STATE.playing || vkAudio.getPlayingState() == VkAudio.PLAYING_STATE.paused)
                this.audioList.get(i).setPlayingState(VkAudio.PLAYING_STATE.stopped);
        }
    }

    public void setSongPaused(int audioId){
        setAllNotPlaying();
        for(int i=0;i<this.audioList.size();i++){
            VkAudio vkAudio = this.audioList.get(i);
            if(vkAudio.id == audioId){
                vkAudio.setPlayingState(VkAudio.PLAYING_STATE.paused);
                notifyDataSetChanged();
                break;

            }
        }
    }

    public void setSongResumed(int currentAudioId) {
        setAllNotPlaying();
        for(int i=0;i<this.audioList.size();i++){
            VkAudio vkAudio = this.audioList.get(i);
            if(vkAudio.id == currentAudioId){
                vkAudio.setPlayingState(VkAudio.PLAYING_STATE.playing);
                notifyDataSetChanged();
                break;

            }
        }
    }

    public void setIsSearching(boolean isSearching){
        this.isSearching = isSearching;
    }

    public List<VkAudio> getMusicList(){
        List<VkAudio> musicList = new ArrayList<VkAudio>();
        if(this.audioList != null && this.audioList.size() > 0)
            musicList.addAll(this.audioList);
        if(this.searchAudioList != null && this.searchAudioList.size() > 0)
            musicList.addAll(this.searchAudioList);
        return musicList;
    }


    @Override
    public int getCount() {
        int count = 0;
        if(audioList == null)
            return 0;
        count = audioList.size();
        if(searchAudioList != null && searchAudioList.size() > 0)
            count += searchAudioList.size();
        return count;
    }

    @Override
    public VkAudio getItem(int position) {
        if(audioList == null)
            return null;
        if(audioList.size() > position)
            return audioList.get(position);
        else if(searchAudioList.size() > position - audioList.size() - 1)
            return searchAudioList.get(position - audioList.size() - 1);
        else
            return null;
    }

    @Override
    public int getViewTypeCount() {
//        if(isSearching)
            return 2;
//        return 1;
    }

    @Override
    public int getItemViewType(int position) {
//        if(isSearching){
            if(position == 0 && isSearching)
                return TYPE_TITLE;
            else {
                if(audioList.size() > position)
                    return TYPE_SONG;
                else {
                    if(searchAudioList.size() > 0 && audioList.size() == position)
                        return TYPE_TITLE;
                    else
                        return TYPE_SONG;
                }
            }
//        }
//        else{
//            return TYPE_SONG;
//        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        int type = getItemViewType(position);

        if(type == TYPE_SONG) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = mInflater.inflate(R.layout.adapter_listmusic_item, null);
                holder.songNameTv = (TextView) convertView.findViewById(R.id.songname_tv);
                holder.artistNameTv = (TextView) convertView.findViewById(R.id.artist_name_tv);
                holder.durationTv = (TextView) convertView.findViewById(R.id.song_duration_tv);
                holder.songImg = (ImageView) convertView.findViewById(R.id.song_img);
                holder.songNameTv.setTypeface(FontUtils.getSongNameFont(mContext));
                holder.artistNameTv.setTypeface(FontUtils.getArtistFont(mContext));
                holder.songPopupImg = (ImageView) convertView.findViewById(R.id.song_popup_img);
                holder.savedSongImg = (ImageView) convertView.findViewById(R.id.saved_song);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            final VkAudio item = getItem(position);

            holder.songPopupImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPopup(v, item);
                }
            });
            holder.songNameTv.setText(item.title);
            holder.artistNameTv.setText(item.artist);
            holder.durationTv.setText(DateUtils.secondsToMinutes(item.duration));
            holder.songImg.setTag(item);
            if(item.isSaved)
                holder.savedSongImg.setVisibility(View.VISIBLE);
            else
                holder.savedSongImg.setVisibility(View.GONE);
//        holder.songImg.setOnClickListener(songClickListener);
            switch (item.getPlayingState()) {
                case playing:
                    holder.songImg.setVisibility(View.VISIBLE);
                    holder.songImg.setBackgroundResource(R.drawable.pausebutton);
                    break;
                case paused:
                    holder.songImg.setVisibility(View.VISIBLE);
                    holder.songImg.setBackgroundResource(R.drawable.playbutton);
                    break;
                case stopped:
                    holder.songImg.setVisibility(View.GONE);
                    break;
            }
        }
        else{
            TitleHolder tH = null;
            if(convertView == null){
                tH = new TitleHolder();
                convertView = mInflater.inflate(R.layout.playlist_item_title_layout, null);
                tH.titleTv = (TextView) convertView.findViewById(R.id.playlist_title);
                convertView.setTag(tH);
            }
            else{
                tH = (TitleHolder) convertView.getTag();
            }
            if(position == 0 && audioList.size() > 0)
                tH.titleTv.setText(mContext.getString(R.string.my_audios));
            else
                tH.titleTv.setText(mContext.getString(R.string.vk_audios));
        }

        return convertView;
    }



    public static class ViewHolder{
        TextView songNameTv;
        TextView artistNameTv;
        TextView durationTv;
        public ImageView songImg;
        ImageView songPopupImg;
        ImageView savedSongImg;
    }

    public static class TitleHolder {
        TextView titleTv;
    }

    private void showPopup(View v, final VkAudio item){
        PopupMenu popup = new PopupMenu(mContext, v);


        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch(menuItem.getItemId()){
                    case R.id.song_download:
                        downloadSong(item);
                        break;
                    case R.id.add_to_my:
                        addToMy(item);
                        break;
                }
                return false;
            }
        });
        popup.inflate(R.menu.list_song_actions);
        MenuItem menuItem = popup.getMenu().findItem(R.id.add_to_my);
        if(VKSdk.getAccessToken().userId.equals(String.valueOf(item.owner_id))){
            menuItem.setVisible(false);
        }
        else
            menuItem.setVisible(true);
        popup.show();
    }

    private void addToMy(VkAudio item){
        AddSongToMyRequest addSongToMyRequest = new AddSongToMyRequest(mContext, item, new OnRequestListener() {
            @Override
            public void onNetworkComplete(List result) {

            }

            @Override
            public void onError() {

            }

            @Override
            public void OnProgress(int progress) {

            }

            @Override
            public void onDbComplete(List result) {

            }
        });
        addSongToMyRequest.execute();
    }


    private void downloadSong(VkAudio item){
        if(!item.isSaved) {
            Intent downloadService = new Intent(mContext, DownloadIntentService.class);
            downloadService.putExtra("vkaudio", item);
            mContext.startService(downloadService);
        }
        else{
            Toast.makeText(mContext, mContext.getString(R.string.song_already_downloaded), Toast.LENGTH_LONG).show();
        }
    }
}
