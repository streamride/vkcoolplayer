package com.streamride.mymoodstream.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.streamride.moodstream.R;
import com.streamride.mymoodstream.models.VkGroup;

import java.util.List;

/**
 * Created by andreyzakharov on 31.07.14.
 */
public class GroupAdapter extends BaseAdapter {

    private final LayoutInflater mInflater;
    private final Context context;
    private List<VkGroup> groupList;


    public GroupAdapter(Context context){
        mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    public void setData(List<VkGroup> groupList){
        if(this.groupList != null)
            this.groupList.clear();
        this.groupList = groupList;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if(groupList == null)
            return 0;
        return groupList.size();
    }

    @Override
    public VkGroup getItem(int position) {
        if(groupList == null)
            return null;
        return groupList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if(convertView == null){
            convertView = mInflater.inflate(R.layout.group_item_layout, null);
            viewHolder = new ViewHolder();
            viewHolder.groupName = (TextView) convertView.findViewById(R.id.group_name_tv);
            viewHolder.groupPhoto = (ImageView) convertView.findViewById(R.id.group_photo_img);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }


        VkGroup vkGroup = getItem(position);
        viewHolder.groupName.setText(vkGroup.name);
        Picasso.with(context).load(vkGroup.photo_100).placeholder(R.drawable.no_cover).into(viewHolder.groupPhoto);


        return convertView;
    }

    static class ViewHolder {
        TextView groupName;
        ImageView groupPhoto;
    }

}
