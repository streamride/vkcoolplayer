package com.streamride.mymoodstream.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.squareup.picasso.Picasso;
import com.streamride.moodstream.R;
import com.streamride.mymoodstream.models.TrackInfo;
import com.streamride.mymoodstream.models.VkAudio;
import com.streamride.mymoodstream.network.services.DetailIntentService;
import com.streamride.mymoodstream.services.MusicContainer;
import com.streamride.mymoodstream.ui.fragment.DetailFragment;
import com.streamride.mymoodstream.utils.L;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by andreyzakharov on 19.05.14.
 */
public class MusicPagerAdapter extends PagerAdapter {


    private final Context context;
    private final LayoutInflater mInflater;
//    private List<VkAudio> vkAudiolist;
    private int currentPosition;
    private Map<Integer, View> mapView = new HashMap<Integer, View>();

    public MusicPagerAdapter(Context context){
        this.context = context;
        mInflater = LayoutInflater.from(context);
    }


//    public void setData(List<VkAudio> vkAudioList){
//        this.vkAudiolist = vkAudioList;
//        notifyDataSetChanged();
//    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        DetailFragment fragment = (DetailFragment) mapView.get(position);
        if(fragment == null){
            fragment = new DetailFragment(context);

            mapView.put(position, fragment);
        }
        container.addView(fragment);
        L.debug("instantiate = "+  position);
        getTrackInfo(position);
        return fragment;
    }




    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ((ViewPager) container).removeView((View) object);
    }


    @Override
    public int getItemPosition(Object object) {
        return POSITION_UNCHANGED;
    }

    @Override
    public int getCount() {
        return MusicContainer.getInstance().getMusicList().size();
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view == o;
    }

    private void getTrackInfo(final int position){
        VkAudio vkAudio = MusicContainer.getInstance().getMusicByPosition(position);
        L.debug("get track info=" +position);
        Intent intent = new Intent(context, DetailIntentService.class);
        intent.putExtra("artist", vkAudio.artist);
        intent.putExtra("title", vkAudio.title);
        intent.putExtra("lyricsId", vkAudio.lyrics_id);
        intent.putExtra("receiver", new ResultReceiver(new Handler()){
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                super.onReceiveResult(resultCode, resultData);
                if(resultCode == 1){
                    TrackInfo trackInfo = (TrackInfo) resultData.getSerializable("trackinfo");
                    if(trackInfo != null)
                        setData(trackInfo, position);

                }
                hideProgress(position);
            }
        });
        context.startService(intent);

    }

    private void setData(TrackInfo trackInfo, int position){
        DetailFragment fragment = (DetailFragment) mapView.get(position);
        if(TextUtils.isEmpty(trackInfo.albumTitle)){
            fragment.getName().setVisibility(View.GONE);
        }
        else
            fragment.getName().setText(trackInfo.artist + " - " + trackInfo.albumTitle);
        fragment.getDetailPb().setVisibility(View.GONE);
        if(TextUtils.isEmpty(trackInfo.imageExtraLarge))
            Picasso.with(context).load(R.drawable.no_cover).into(fragment.getAlbumImage());
        else {
            Picasso.with(context).load(trackInfo.imageExtraLarge).error(R.drawable.no_cover).into(fragment.getAlbumImage());
            Picasso.with(context).load(trackInfo.imageExtraLarge).into(fragment.getBlurImage1());
            Picasso.with(context).load(trackInfo.imageExtraLarge).into(fragment.getBlurImage2());
        }

        fragment.getSongLyrics().setText(trackInfo.lyrics);
//        fragment.getAdsWv().loadUrl(C.test_url);
////        fragment.getAdsWv().loadData(C.test_url, "text/html",
////                "utf-8");
//        fragment.getAdsWv().setVisibility(View.VISIBLE);
    }


    private void hideProgress(int position){
        DetailFragment fragment = (DetailFragment) mapView.get(position);
        fragment.getDetailPb().setVisibility(View.GONE);
    }



}
