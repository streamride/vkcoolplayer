package com.streamride.mymoodstream.ui;

import android.os.Bundle;
import android.preference.PreferenceScreen;
import android.support.v4.app.Fragment;
import android.support.v4.preference.PreferenceFragment;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import com.streamride.moodstream.R;
import com.streamride.mymoodstream.ui.fragment.SettingsFragment;

/**
 * Created by andreyzakharov on 23.05.14.
 */
public class PreferenceActivity extends ActionBarActivity implements PreferenceFragment.OnPreferenceAttachedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle(getString(R.string.str_settings));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Fragment fragment = new SettingsFragment();
        getSupportFragmentManager().beginTransaction().replace(android.R.id.content, fragment).commit();
    }


    @Override
    public void onPreferenceAttached(PreferenceScreen root, int xmlId) {

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
