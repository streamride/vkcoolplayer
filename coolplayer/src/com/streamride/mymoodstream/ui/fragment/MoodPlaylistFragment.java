package com.streamride.mymoodstream.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
//import com.parse.FindCallback;
//import com.parse.ParseException;
//import com.parse.ParseObject;
//import com.parse.ParseQuery;
import com.streamride.moodstream.R;
import com.streamride.mymoodstream.models.Playlist;
import com.streamride.mymoodstream.ui.adapters.PlaylistAdapter;

import java.util.List;

/**
 * Created by andreyzakharov on 30.04.14.
 */
public class MoodPlaylistFragment extends Fragment {

    private View rootView;
    private GridView playlistGrid;
    private PlaylistAdapter playlistAdapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        playlistAdapter = new PlaylistAdapter(getActivity());
//        loadPlaylist();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.mood_playlist_layout, null);

        playlistGrid = (GridView)rootView.findViewById(R.id.playlist_grid);
        playlistGrid.setAdapter(playlistAdapter);

        return rootView;
    }



//    private void loadPlaylist(){
//        ParseQuery<ParseObject> query = ParseQuery.getQuery("playlist");
//        query.findInBackground(new FindCallback<ParseObject>() {
//            @Override
//            public void done(List<ParseObject> parseObjects, ParseException e) {
//                setData(parseObjects);
//            }
//        });
//    }
//
//    private void setData(List<ParseObject> parseObjectList){
//        List<Playlist> playlistList = Playlist.getListFromParse(parseObjectList);
//        playlistAdapter.setData(playlistList);
//    }
}
