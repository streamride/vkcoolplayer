package com.streamride.mymoodstream.ui.fragment;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.streamride.moodstream.R;
import com.streamride.mymoodstream.utils.FontUtils;
import com.streamride.mymoodstream.widgets.AdsWebView;

/**
 * Created by andreyzakharov on 30.05.14.
 */
public class DetailFragment extends FrameLayout {
    private LayoutInflater mInflater;
    private ImageView albumImage;
    private TextView name;
    private TextView songLyrics;
    private ProgressBar detailPb;
    private ImageView blurImage1;
    private ImageView blurImage2;
    private AdsWebView adsWv;

    public DetailFragment(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public DetailFragment(Context context, AttributeSet attrs){
        this(context, attrs, 0);
    }

    public DetailFragment(Context context){
        this(context, null, 0);
    }

    private void init(){
        mInflater = LayoutInflater.from(getContext());
        View view = mInflater.inflate(R.layout.music_detail_layout,null);
        albumImage = (ImageView) view.findViewById(R.id.albumImage);
        blurImage1 = (ImageView) view.findViewById(R.id.albumBlur1);
        blurImage2 = (ImageView) view.findViewById(R.id.albumBlur2);
        adsWv = (AdsWebView) view.findViewById(R.id.ads);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            blurImage1.setImageAlpha(50);
            blurImage2.setImageAlpha(50);
        }
        else{
            blurImage1.setAlpha(50);
            blurImage2.setAlpha(50);
        }
        name = (TextView) view.findViewById(R.id.name);
        songLyrics = (TextView) view.findViewById(R.id.songLyrics);
        songLyrics.setTypeface(FontUtils.getSongNameFont(getContext()));
        detailPb = (ProgressBar) view.findViewById(R.id.detail_pb);
        addView(view);
    }

    public ImageView getAlbumImage(){
        return albumImage;
    }

    public ImageView getBlurImage1(){
        return blurImage1;
    }

    public ImageView getBlurImage2(){
        return blurImage2;
    }

    public TextView getName(){
        return name;
    }

    public TextView getSongLyrics(){
        return songLyrics;
    }

    public ProgressBar getDetailPb(){
        return detailPb;
    }

    public AdsWebView getAdsWv(){
        return adsWv;
    }

}
