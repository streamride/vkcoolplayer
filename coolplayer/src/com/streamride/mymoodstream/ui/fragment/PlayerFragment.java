package com.streamride.mymoodstream.ui.fragment;

import android.content.*;
import android.os.*;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.*;
import android.widget.*;
import com.appsflyer.AppsFlyerLib;
import com.streamride.moodstream.R;
import com.streamride.mymoodstream.models.VkAudio;
import com.streamride.mymoodstream.network.impl.DownloadIntentService;
import com.streamride.mymoodstream.services.MusicContainer;
import com.streamride.mymoodstream.services.PlayerService;
import com.streamride.mymoodstream.services.UIPlayerProcessor;
import com.streamride.mymoodstream.ui.adapters.MusicPagerAdapter;
import com.streamride.mymoodstream.utils.*;
import com.streamride.mymoodstream.widgets.AdsWebView;
import com.streamride.mymoodstream.widgets.DepthPageTransformer;

/**
 * Created by andreyzakharov on 03.04.14.
 */
public class PlayerFragment extends Fragment {


    private View rootView;
    private View playerWidget;
    private ImageView pPlayPauseBtn;
    private TextView pSongName;
    private TextView pArtistName;
    private TextView pDuration;
    private SeekBar pSeekBar;
    private ImageView pShuffleBtn;
    private ImageView pRepeatBtn;
    private ImageView pNextBtn;
    private ImageView pPrevBtn;
    private ViewPager mViewPager;
    private boolean isSwipe = true;
    private MusicPagerAdapter musicPagerAdapter;
    private AdsWebView adsWv;
    private TextView pSongDurationReverse;
    private InternetStatus internetStatus;
    private UIPlayerProcessor uiPlayerProcessor;

    public static PlayerFragment newInstance(int position){
        PlayerFragment fragment = new PlayerFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(C.PLAYER.POSITION, position);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        uiPlayerProcessor = new UIPlayerProcessor();
        uiPlayerProcessor.init(getActivity(), playerStateListener);

//        initReceiver();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        playerWidget = inflater.inflate(R.layout.player_widget_layout, null);


        initPlayerWidget();
        mViewPager = (ViewPager) playerWidget.findViewById(R.id.song_info_vp);
        internetStatus = new InternetStatus(getActivity());
//        if(internetStatus.isOnline() && AdsHelper.INSTANCE.isShowAds(getActivity())) {
//            adsWv = (AdsWebView) playerWidget.findViewById(R.id.ads);
//            adsWv.setVisibility(View.VISIBLE);
//            adsWv.loadUrl(C.test_url);
//            new ReloadWebView(getActivity(), adsWv);
//        }
        setViewPager(PlayerService.currentPosition);
        return playerWidget;
    }

    private void showPlayer(VkAudio vkAudio){
        playerWidget.setVisibility(View.VISIBLE);
        pArtistName.setText(vkAudio.artist);
        pSongName.setText(vkAudio.title);
        pDuration.setText(DateUtils.secondsToMinutes(vkAudio.duration));
        pSeekBar.setMax(vkAudio.duration);
        if(PlayerService.state == C.PLAYER_STATES.PLAYING)
            playerButtonPause();
        else
            playerButtonPlay();
    }

    private void updatePlayerSeekBar(int progress){
        pSeekBar.setSecondaryProgress(progress);
    }

    private void uiUpdatePlayerPosition(int position){
        pSeekBar.setProgress(position);
        int secondsLeft = pSeekBar.getMax() - position;
        pDuration.setText(DateUtils.secondsToMinutes(position));
        pSongDurationReverse.setText("-" + DateUtils.secondsToMinutes(secondsLeft));

    }

//    BroadcastReceiver mMusicReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            if(C.ACTIONS.NEW_SONG.equals(intent.getAction())){
//                checkPlayerPlaying();
////                AppsFlyerLib.sendTrackingWithEvent(getActivity().getApplicationContext(), "new_song", "");
//            }
//            else if(C.ACTIONS.BUFFER.equals(intent.getAction())){
//                int buffer = intent.getIntExtra(C.PLAYER.BUFFER,0);
//                updatePlayerSeekBar(buffer);
//            }
//            else if(C.ACTIONS.CURRENT_POSITION.equals(intent.getAction())){
//                int curPosition = intent.getIntExtra(C.PLAYER.CURRENT_TIME,0);
//                updatePlayerPosition(curPosition);
//            }
//            else if(C.ACTIONS.PLAYER_STATE.equals(intent.getAction())){
//                int state = intent.getIntExtra(C.PLAYER.STATE,0);
//                if(state == C.PLAYER_STATES.PLAYING)
//                    playerButtonPause();
//                else if(state == C.PLAYER_STATES.PAUSED)
//                    playerButtonPlay();
//            }
//        }
//    };


//    private void initReceiver(){
//        IntentFilter filters = new IntentFilter();
//        filters.addAction(C.ACTIONS.BUFFER);
//        filters.addAction(C.ACTIONS.CURRENT_POSITION);
//        filters.addAction(C.ACTIONS.NEW_SONG);
//        filters.addAction(C.ACTIONS.PLAYER_STATE);
//        getActivity().registerReceiver(mMusicReceiver, filters);
//    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.full_player_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_download:
                downloadSong(PlayerService.currentSong);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void downloadSong(VkAudio item){
        if(!item.isSaved) {
            Intent downloadService = new Intent(getActivity(), DownloadIntentService.class);
            downloadService.putExtra("vkaudio", item);
            getActivity().startService(downloadService);
        }
        else{
            Toast.makeText(getActivity(), getActivity().getString(R.string.song_already_downloaded), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        uiPlayerProcessor.connectToPlayerService();
    }

    private void initPlayerWidget(){
        pPlayPauseBtn = (ImageView) playerWidget.findViewById(R.id.play_btn);
        pSongName = (TextView) playerWidget.findViewById(R.id.current_songname_tv);
        pArtistName = (TextView) playerWidget.findViewById(R.id.current_artist_tv);
        pDuration = (TextView) playerWidget.findViewById(R.id.current_duration_song);
        pSeekBar = (SeekBar) playerWidget.findViewById(R.id.player_seekBar);
        pSongDurationReverse = (TextView) playerWidget.findViewById(R.id.current_duration_song_reverse);
        pSongName.setTypeface(FontUtils.getSongNameFont(getActivity()));
        pArtistName.setTypeface(FontUtils.getArtistFont(getActivity()));
        pShuffleBtn = (ImageView) playerWidget.findViewById(R.id.shuffle_btn);
        pRepeatBtn = (ImageView) playerWidget.findViewById(R.id.repeat_btn);
        pNextBtn = (ImageView) playerWidget.findViewById(R.id.next_btn);
        pPrevBtn = (ImageView) playerWidget.findViewById(R.id.prev_btn);
        pRepeatBtn.setOnClickListener(playerClickListener);
        pNextBtn.setOnClickListener(playerClickListener);
        pPrevBtn.setOnClickListener(playerClickListener);
        pShuffleBtn.setOnClickListener(playerClickListener);
        pSeekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                float x = event.getX();
                float y = event.getY();
                if(y > x){
                    return true;
                }
                return false;
            }
        });
        pSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser)
                    uiPlayerProcessor.seekSong(progress);
//                    seekSong(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        pPlayPauseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(PlayerService.state == C.PLAYER_STATES.PLAYING) {
//                    pauseSong();
                    uiPlayerProcessor.pauseSong();
                    playerButtonPlay();
                }
                else {
//                    resumeSong();
                    uiPlayerProcessor.resumeSong();
                    playerButtonPause();

                }
            }
        });
    showPlayer(PlayerService.currentSong);
    }

    private boolean setViewPager(int position){
        musicPagerAdapter = new MusicPagerAdapter(getActivity());
        mViewPager.setAdapter(musicPagerAdapter);

//       musicPagerAdapter.setData(listMusicFragment.getMusicList());
        mViewPager.setCurrentItem(position, false);
        musicPagerAdapter.notifyDataSetChanged();
        mViewPager.setPageTransformer(true, new DepthPageTransformer());

        mViewPager.setOnPageChangeListener(onpageselected);
        return true;
    }

//    private void resumeSong(){
//        if (!mBound)
//            return;
//        Bundle bundle = new Bundle();
////        bundle.putInt(C.PLAYER.AUDIO_ID, audio_id);
//        Message msg = Message.obtain(null, PlayerService.RESUME);
//        msg.setData(bundle);
//        try {
//            mService.send(msg);
//        } catch (RemoteException e) {
//            e.printStackTrace();
//        }
//    }


//    private void pauseSong(){
//        if (!mBound)
//            return;
//        Bundle bundle = new Bundle();
//        Message msg = Message.obtain(null, PlayerService.PAUSE);
//        msg.setData(bundle);
//        try {
//            mService.send(msg);
//        } catch (RemoteException e) {
//            e.printStackTrace();
//        }
//    }


    private View.OnClickListener playerClickListener = new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.repeat_btn:
                    toggleRepeat();
                    break;
                case R.id.shuffle_btn:
//                    setShuffle();
                    uiPlayerProcessor.setShuffle();
                    break;
                case R.id.next_btn:
//                    stopPlayer();
                    setSwipe(false);
                    nextSong();

                    break;
                case R.id.prev_btn:
//                    stopPlayer();
                    setSwipe(false);
                    prevSong();
                    break;
            }
        }
    };

    private void toggleRepeat(){
        if(PlayerService.mRepeat){
            pRepeatBtn.setImageResource(R.drawable.repeat);
            PlayerService.mRepeat = false;
        }
        else{
            pRepeatBtn.setImageResource(R.drawable.repeat_blue);
            PlayerService.mRepeat = true;
        }
    }

//    private void setShuffle(){
//        sendMessage(null, PlayerService.SHUFFLE);
//    }

    private void setSwipe(boolean isSwipe){
        this.isSwipe = isSwipe;
    }

    private void nextSong(){
        uiPlayerProcessor.nextSong();
        mViewPager.setCurrentItem(PlayerService.currentPosition);
//        sendMessage(null, PlayerService.NEXT);
    }

    private void prevSong(){
//        sendMessage(null, PlayerService.PREV);
        uiPlayerProcessor.prevSong();
        mViewPager.setCurrentItem(PlayerService.currentPosition);
    }

//    private void sendMessage(Bundle bundle, int what){
//        if(!mBound)
//            return;
//        Message msg = Message.obtain(null,what);
//        if(bundle != null)
//            msg.setData(bundle);
//        try {
//            mService.send(msg);
//        } catch (RemoteException e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    private Messenger mService;
//    private boolean mBound;
//    private ServiceConnection mConnection = new ServiceConnection() {
//
//        @Override
//        public void onServiceDisconnected(ComponentName arg0) {
//            mService = null;
//            mBound = false;
//        }
//
//        @Override
//        public void onServiceConnected(ComponentName name, IBinder service) {
//            mService = new Messenger(service);
//            mBound = true;
//            checkPlayerPlaying();
//        }
//    };


    private void checkPlayerPlaying(){

        if(PlayerService.state == C.PLAYER_STATES.PLAYING || PlayerService.state == C.PLAYER_STATES.PREPARING){
            VkAudio vkAudio = PlayerService.currentSong;
//            pSeekBar.setProgress(0);
            if(vkAudio != null) {
                showPlayer(vkAudio);
//                setSwipe(false);
                if(!isSwipe)
                    mViewPager.setCurrentItem(PlayerService.currentPosition, false);
            }
        }
    }


    private void playerButtonPlay(){
        pPlayPauseBtn.setBackgroundResource(R.drawable.play);
    }

    private void playerButtonPause(){
        pPlayPauseBtn.setBackgroundResource(R.drawable.pause);
    }




    ViewPager.OnPageChangeListener onpageselected = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i2) {

        }

        @Override
        public void onPageSelected(final int i) {
            if(isSwipe) {
                uiPlayerProcessor.stopPlayer();
                uiPlayerProcessor.playSong(i, MusicContainer.getInstance().getMusicByPosition(i));
//                playSong(i, MusicContainer.getInstance().getMusicByPosition(i));
            }
            setSwipe(true);
        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };


    @Override
    public void onDestroy() {
        super.onDestroy();
        uiPlayerProcessor.destroy();
//        getActivity().unregisterReceiver(mMusicReceiver);
//        if(mBound)
//            getActivity().unbindService(mConnection);
    }


    public UIPlayerProcessor.PlayerStateListener playerStateListener = new UIPlayerProcessor.PlayerStateListener() {
        @Override
        public void updatePlayerBuffer(int buffer) {
            updatePlayerSeekBar(buffer);
        }

        @Override
        public void changePlayerState(int state) {
                if(state == C.PLAYER_STATES.PLAYING)
                    playerButtonPause();
                else if(state == C.PLAYER_STATES.PAUSED)
                    playerButtonPlay();
        }

        @Override
        public void updatePlayerPosition(int position) {
            uiUpdatePlayerPosition(position);
        }

        @Override
        public void newSong() {
            checkPlayerPlaying();
        }

        @Override
        public void onServiceConnected() {
            checkPlayerPlaying();
        }
    };


}
