package com.streamride.mymoodstream.ui.fragment;

import android.content.*;
import android.database.Cursor;
import android.os.*;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.*;
import android.widget.*;
import com.streamride.moodstream.R;
import com.streamride.mymoodstream.db.DBContentProvider;
import com.streamride.mymoodstream.network.impl.AudioRequest;
import com.streamride.mymoodstream.network.impl.SearchVkRequest;
import com.streamride.mymoodstream.network.loaders.MusicLoader;
import com.streamride.mymoodstream.network.services.MusicIntentService;
import com.streamride.mymoodstream.services.MusicContainer;
import com.streamride.mymoodstream.ui.MainActivity;
import com.streamride.mymoodstream.ui.adapters.ListMusicAdapter;
import com.streamride.mymoodstream.models.VkAudio;
import com.streamride.mymoodstream.network.OnRequestListener;
import com.streamride.mymoodstream.services.PlayerService;
import com.streamride.mymoodstream.utils.C;
import com.streamride.mymoodstream.utils.InternetStatus;
import com.streamride.mymoodstream.utils.L;
import com.streamride.mymoodstream.utils.QueryBuilder;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by andreyzakharov on 03.04.14.
 */
public class ListMusicFragment extends Fragment implements AdapterView.OnItemClickListener, Filterable, LoaderCallbacks<List<VkAudio>>{


    private static final long DELAY_SEARCH = 50;
    private View rootView;
    private ListMusicAdapter mAdapter;
    private ListView mMusicListView;
    private FrameLayout playerWidget;
    private ImageView pPlayPauseBtn;
    private TextView pSongName;
    private TextView pArtistName;
    private TextView pDuration;
    private SeekBar pSeekBar;
    private String mUrl;
    private String mPlaylist;
    private int mAlbumdId = 0;
    private Filter mFilter;
    private Context mContext;
    private SearchHandler searchHandler;
    private AudioRequest audioRequest;
    private List<VkAudio> musicList;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private InternetStatus internetStatus;
    private MainActivity.StartServiceListener startServiceListener;

    public static ListMusicFragment newInstance(String url, String playlist, int albumId){
        ListMusicFragment fragment = new ListMusicFragment();
        Bundle bundle = new Bundle();
        bundle.putString("url", url);
        bundle.putString("playlist", playlist);
        bundle.putInt("album_id", albumId);
        fragment.setArguments(bundle);

        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mContext = getActivity();
        mUrl = getArguments().getString("url");
        mPlaylist = getArguments().getString("playlist");
        if(getArguments().containsKey("album_id"))
            mAlbumdId = getArguments().getInt("album_id");

        internetStatus = new InternetStatus(getActivity());


        mAdapter = new ListMusicAdapter(getActivity());

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void setReceiver(){
        IntentFilter filters = new IntentFilter();
        filters.addAction(C.ACTIONS.NEW_SONG);
        filters.addAction(C.ACTIONS.BUFFER);
        filters.addAction(C.ACTIONS.CURRENT_POSITION);
        filters.addAction(C.ACTIONS.PLAYER_STATE);
        filters.addAction(C.ACTIONS.SONG_DOWNLOADED);
        getActivity().registerReceiver(mMusicReceiver, filters);
    }

    @Override
    public void onResume() {
        super.onResume();
        setReceiver();
        Intent intent = new Intent(getActivity(), PlayerService.class);
        getActivity().startService(intent);
        getActivity().bindService(intent, mConnection,Context.BIND_AUTO_CREATE);

    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(mMusicReceiver);
//        audioRequest.destroy();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(mBound)
            getActivity().unbindService(mConnection);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadMusic(true);
        L.debug("onactivitycreated");
    }

    @Override
    public void onStart() {
        super.onStart();
        L.debug("onstart");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        L.debug("oncreateview");
        rootView = inflater.inflate(R.layout.fragment_listmusic_layout,null);
        mMusicListView = (ListView) rootView.findViewById(R.id.song_listview);
//        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.refresh);
//        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                update(mUrl, mPlaylist, mAlbumdId);
//                mSwipeRefreshLayout.setRefreshing(true);
//            }
//        });
//        mSwipeRefreshLayout.setColorScheme(R.color.blue, R.color.green, R.color.yellow, R.color.red);
        mMusicListView.setAdapter(mAdapter);
        mMusicListView.setOnItemClickListener(this);

        return rootView;
    }

    public void update(String url, String playlist, int album_id){
        mUrl = url;
        mPlaylist = playlist;
        mAlbumdId = album_id;
        loadMusic(true);
    }

    
    public List<VkAudio> getMusicList(){
        return mAdapter.getMusicList();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        L.debug("ondestroy");


    }

    private void loadMusic(boolean isNeedFromNetwork){
        Bundle bundle = new Bundle();
        bundle.putBoolean(C.LOAD.IS_NETWORK_NEED, isNeedFromNetwork);
        if(isAdded()) {
            getLoaderManager().restartLoader(0, bundle, this);
            if (isNeedFromNetwork)
                showLoading();
        }
    }


    private void loadMusicFromNetwork(){
        Intent musicIntentService = new Intent(getActivity(), MusicIntentService.class);
        musicIntentService.putExtra("playlist", mPlaylist);
        musicIntentService.putExtra("url", mUrl);
        musicIntentService.putExtra("receiver", new ResultReceiver(null){
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                super.onReceiveResult(resultCode, resultData);
                if(resultCode == 1){
                   loadMusic(false);
                }
            }
        });
        getActivity().startService(musicIntentService);
    }


    
    public void setData(List<VkAudio> musicList){
        this.musicList = musicList;
        mAdapter.setMusicList(musicList);
    }

    private void showLoading(){
        getActivity().setProgressBarIndeterminateVisibility(true);
//        if(mSwipeRefreshLayout != null)
//            mSwipeRefreshLayout.setRefreshing(true);

    }

    private void hideLoading(){
        FragmentActivity activity = getActivity();
//        mSwipeRefreshLayout.setRefreshing(false);
        if(activity != null)
            activity.setProgressBarIndeterminateVisibility(false);
    }


    public void updatePauseSong(){
        mAdapter.setSongPaused(PlayerService.currentAudioId);
    }

    public void updateResumeSong(){
        mAdapter.setSongResumed(PlayerService.currentAudioId);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        VkAudio vkAudio = (VkAudio) parent.getItemAtPosition(position);

        if(vkAudio != null){

            if(vkAudio.getPlayingState() == VkAudio.PLAYING_STATE.stopped){
                playSong(position,vkAudio);
                updatePlayingSong(vkAudio.id);
            }
            else if(vkAudio.getPlayingState() == VkAudio.PLAYING_STATE.paused){
                resumeSong();
                updatePlayingSong(vkAudio.id);
            }
            else{
                pauseSong();
                updatePauseSong();
            }
        }

    }

    private void updatePlayingSong(int audioId){
        mAdapter.updateById(audioId);
        L.debug("updateplayingsong");
    }


    private void checkPlayerPlaying(){
        if(PlayerService.state == C.PLAYER_STATES.PLAYING){
            updatePlayingSong(PlayerService.currentAudioId);
        }
    }



    private void playSong(int position, VkAudio vkAudio) {
        if (!mBound)
            return;
        Bundle bundle = new Bundle();
        bundle.putInt(C.PLAYER.AUDIO_ID, vkAudio.id);
        bundle.putInt(C.PLAYER.POSITION, position);
        MusicContainer.getInstance().setMusicList(getMusicList());
//        bundle.putIntegerArrayList(C.PLAYER.AUDIO_ID_ARRAY, mAdapter.getAudioIds());

        sendMessage(bundle,PlayerService.PLAY);
    }


    private void pauseSong(){
        if (!mBound)
            return;
        L.debug("pause");

        sendMessage(null,PlayerService.PAUSE);
        mAdapter.setSongPaused(PlayerService.currentAudioId);
    }



    private void resumeSong(){
        if (!mBound)
            return;

        sendMessage(null,PlayerService.RESUME);
        mAdapter.setSongResumed(PlayerService.currentAudioId);
    }

    private void sendMessage(Bundle bundle, int what){
        Message msg = Message.obtain(null,what);
        if(bundle != null)
            msg.setData(bundle);
        try {
            mService.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }


    private void updateSongSaved(int id){
        if(musicList !=null && musicList.size()>0){
            int size = musicList.size();
            for(int i=0;i<size;i++){
                if(musicList.get(i).id == id){
                    musicList.get(i).isSaved = true;
                    mAdapter.notifyDataSetChanged();
                    break;
                }
            }
        }
    }


    private Messenger mService;
    private boolean mBound = false;
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mService = null;
            mBound = false;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = new Messenger(service);
            mBound = true;

        }
    };

    BroadcastReceiver mMusicReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(C.ACTIONS.NEW_SONG.equals(intent.getAction())){
                int newAudioId = intent.getIntExtra(C.PLAYER.AUDIO_ID,0);
                updatePlayingSong(newAudioId);
            }
            else if(C.ACTIONS.PLAYER_STATE.equals(intent.getAction())){
                int state = intent.getIntExtra(C.PLAYER.STATE,0);
                if(state == C.PLAYER_STATES.PLAYING)
                    updateResumeSong();
                else if(state == C.PLAYER_STATES.PAUSED) {
                    L.debug("paused song");
                    updatePauseSong();
                }
            }
            else if(C.ACTIONS.SONG_DOWNLOADED.equals(intent.getAction())){
                int audioId = intent.getIntExtra(VkAudio.ID,0);
                updateSongSaved(audioId);
            }

        }
    };

    public void searchSong(String str){
        mAdapter.setIsSearching(true);
        getFilter().filter(str);
        if(searchHandler == null)
            searchHandler = new SearchHandler(this);
        Message msg = new Message();
        msg.what = 0;
        msg.obj = str;
        searchHandler.sendMessageDelayed(msg, DELAY_SEARCH);
    }



    public void clearSearch(){
        mAdapter.clearSearch();
    }

    @Override
    public Filter getFilter() {
        if(mFilter == null){
            mFilter = new Filter(){

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    QueryBuilder queryBuilder = new QueryBuilder();
                    queryBuilder.setSearchColumns(VkAudio.ARTIST,VkAudio.TITLE);
                    queryBuilder = queryBuilder.setSearchQuery(constraint.toString());
                    Cursor cursor = queryBuilder.query(mContext, DBContentProvider.CONTENT_URI_AUDIO);
                    List<VkAudio> vkAudioList = VkAudio.initFromCursor(mContext, cursor);
                    filterResults.values = vkAudioList;
                    filterResults.count = vkAudioList.size();
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    List<VkAudio> vkAudioList = (List<VkAudio>) results.values;
                    mAdapter.setSearchData(vkAudioList);
                }
            };
        }

        return mFilter;
    }

    public void updateSong() {
        if(musicList != null && musicList.size() > 0){
            for(int i=0;i<musicList.size();i++){
                if(musicList.get(i).id == PlayerService.currentAudioId){
                    if(PlayerService.state == C.PLAYER_STATES.PAUSED)
                        mAdapter.setSongPaused(PlayerService.currentAudioId);
                    else
                        mAdapter.setSongResumed(PlayerService.currentAudioId);
                }
            }
        }
    }

    @Override
    public Loader<List<VkAudio>> onCreateLoader(int i, Bundle bundle) {
        return new MusicLoader(getActivity(),mPlaylist,mAlbumdId,bundle);
    }

    @Override
    public void onLoadFinished(Loader<List<VkAudio>> listLoader, List<VkAudio> vkAudios) {
        if(mPlaylist.equals(C.PLAYLIST.SAVED) || !internetStatus.isOnline())
            hideLoading();
        setData(vkAudios);
        checkPlayerPlaying();
        MusicLoader musicLoader = (MusicLoader) listLoader;
        Bundle bundle = musicLoader.getBundle();
        boolean isNetworkNeed = bundle.getBoolean(C.LOAD.IS_NETWORK_NEED);
        if(isNetworkNeed)
            loadMusicFromNetwork();
        else
            hideLoading();
    }

    @Override
    public void onLoaderReset(Loader<List<VkAudio>> listLoader) {

    }

    public void setOnStartServiceListener(MainActivity.StartServiceListener startServiceListener) {
        this.startServiceListener = startServiceListener;
    }


    static class SearchHandler extends Handler {
        private WeakReference<ListMusicFragment> mFragment;

        public SearchHandler(ListMusicFragment fragment) {
            this.mFragment = new WeakReference<ListMusicFragment>(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            ListMusicFragment fragment = mFragment.get();
            if (fragment != null) {
                String filterStr = (String) msg.obj;
                fragment.searchVkAudio(filterStr);
            }
            super.handleMessage(msg);
        }
    }

    private void searchVkAudio(String filterStr){
        SearchVkRequest searchVkRequest = new SearchVkRequest(getActivity(),filterStr, new OnRequestListener<VkAudio>() {
            @Override
            public void onNetworkComplete(List<VkAudio> result) {
                mAdapter.setVkSearchData(result);
            }

            @Override
            public void onError() {

            }

            @Override
            public void OnProgress(int progress) {

            }

            @Override
            public void onDbComplete(List result) {

            }
        });
        searchVkRequest.execute();
    }
}