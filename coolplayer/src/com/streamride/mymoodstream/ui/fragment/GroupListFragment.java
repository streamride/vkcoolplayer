package com.streamride.mymoodstream.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.streamride.moodstream.R;
import com.streamride.mymoodstream.models.VkGroup;
import com.streamride.mymoodstream.network.loaders.GroupLoader;
import com.streamride.mymoodstream.network.loaders.MusicLoader;
import com.streamride.mymoodstream.network.services.GroupIntentService;
import com.streamride.mymoodstream.network.services.MusicIntentService;
import com.streamride.mymoodstream.ui.adapters.GroupAdapter;
import com.streamride.mymoodstream.utils.C;
import com.streamride.mymoodstream.utils.L;

import java.util.List;

/**
 * Created by andreyzakharov on 31.07.14.
 */
public class GroupListFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<VkGroup>> {


    private View rootView;
    private ListView mGroupListView;
    private GroupAdapter groupAdapter;

    public static GroupListFragment newInstance(){
        GroupListFragment fragment = new GroupListFragment();

        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        groupAdapter = new GroupAdapter(getActivity());
        loadGroups(true);
    }


    private void showLoading(){
        getActivity().setProgressBarIndeterminateVisibility(true);

    }

    private void hideLoading(){
        FragmentActivity activity = getActivity();
        if(activity != null)
            activity.setProgressBarIndeterminateVisibility(false);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_listmusic_layout,null);
        mGroupListView = (ListView) rootView.findViewById(R.id.song_listview);
        mGroupListView.setAdapter(groupAdapter);

        return rootView;
    }

    private void loadGroups(boolean isNeedFromNetwork){
        L.debug(" start load groups ");
        Bundle bundle = new Bundle();
        bundle.putBoolean(C.LOAD.IS_NETWORK_NEED, isNeedFromNetwork);
        if(isAdded()) {
            getLoaderManager().restartLoader(0, bundle, this);
            if (isNeedFromNetwork)
                showLoading();
        }
    }

    @Override
    public Loader<List<VkGroup>> onCreateLoader(int i, Bundle bundle) {
        L.debug(" creating loading");
        return new GroupLoader(getActivity(), bundle);
    }

    @Override
    public void onLoadFinished(Loader<List<VkGroup>> listLoader, List<VkGroup> vkGroupList) {
        L.debug(" load finished ");
        GroupLoader groupLoader = (GroupLoader) listLoader;
        groupAdapter.setData(vkGroupList);
        Bundle bundle = groupLoader.getBundle();
        boolean isNetworkNeed = bundle.getBoolean(C.LOAD.IS_NETWORK_NEED);
        if(isNetworkNeed)
            loadGroupsFromNetwork();
        else
            hideLoading();
    }

    @Override
    public void onLoaderReset(Loader<List<VkGroup>> listLoader) {

    }

    private void loadGroupsFromNetwork(){
        Intent groupIntentService = new Intent(getActivity(), GroupIntentService.class);
        groupIntentService.putExtra("receiver", new ResultReceiver(null){
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                super.onReceiveResult(resultCode, resultData);
                if(resultCode == 1){
                    loadGroups(false);
                }
            }
        });
        getActivity().startService(groupIntentService);
    }

}
