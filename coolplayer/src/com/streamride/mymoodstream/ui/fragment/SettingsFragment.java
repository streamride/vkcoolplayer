package com.streamride.mymoodstream.ui.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.support.v4.preference.PreferenceFragment;
import com.streamride.moodstream.R;
import com.streamride.mymoodstream.utils.GlobalSettings;

/**
 * Created by andreyzakharov on 23.05.14.
 */
public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener,
                    PreferenceFragment.OnPreferenceAttachedListener, Preference.OnPreferenceClickListener{

    private static final String KEY_CHANGE_SHAKE = "change_shake";
    private static final String KEY_CACHE_FOLDER = "cache_folder";
    private SharedPreferences sPrefs;
    private CheckBoxPreference changeShake;
    private Preference folderCache;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.global_settings);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        sPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        PreferenceManager preferenceManager = getPreferenceManager();
        sPrefs.registerOnSharedPreferenceChangeListener(this);
        changeShake = (CheckBoxPreference) preferenceManager.findPreference(KEY_CHANGE_SHAKE);
        changeShake.setChecked(GlobalSettings.GLOBAL_SETTINGS.isChangeSongByShake(getActivity()));
//        folderCache = (Preference) preferenceManager.findPreference(KEY_CACHE_FOLDER);
//        folderCache.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
//            @Override
//            public boolean onPreferenceClick(Preference preference) {
//                selectCacheFolder();
//                return false;
//            }
//        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPreferenceAttached(PreferenceScreen root, int xmlId) {

    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        return false;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
           if(KEY_CACHE_FOLDER.equals(key)){
               selectCacheFolder();
           }
        else if(KEY_CHANGE_SHAKE.equals(key)){
               boolean change_shake = sharedPreferences.getBoolean(KEY_CHANGE_SHAKE, false);
               GlobalSettings.GLOBAL_SETTINGS.setIsChangeSongByShake(getActivity(), change_shake);
           }
    }

    private void selectCacheFolder() {
        Intent intent = new Intent(Intent.ACTION_PICK);
//        Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath()
//                + "/myFolder/");
//        intent.setDataAndType(uri, "text/csv");
        startActivity(Intent.createChooser(intent, "Open folder"));
    }
}
