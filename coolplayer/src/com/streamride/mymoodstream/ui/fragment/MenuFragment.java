package com.streamride.mymoodstream.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.streamride.moodstream.R;
import com.streamride.mymoodstream.ui.MainActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andreyzakharov on 31.07.14.
 */
public class MenuFragment extends Fragment {


    private View rootView;
    private ListView mListView;

    private List<String> menuList = new ArrayList<String>();
    private MenuAdapter mAdapter;


    private void initMenuList(){
        menuList.add("Музыка групп");
        menuList.add("Музыка друзей");
        menuList.add("Музыка со стены");
        menuList.add("Популярное");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initMenuList();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_menu_layout, null);

        mListView = (ListView)rootView.findViewById(R.id.menu_listview);
        mAdapter = new MenuAdapter();
        mListView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
//        mListView.setOnItemClickListener(menuItemClickListener);


        return rootView;
    }

//    private AdapterView.OnItemClickListener menuItemClickListener = new AdapterView.OnItemClickListener() {
//        @Override
//        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//            switch (position){
//                case 0:
//                    ((MainActivity)getActivity()).openFragment(MainActivity.FragmentType.LISTGROUP);
//
//                    break;
//            }
//        }
//    };


    private class MenuAdapter extends BaseAdapter {


        private TextView mMenuItem;

        @Override
        public int getCount() {
            return menuList.size();
        }

        @Override
        public String getItem(int position) {
            return menuList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            convertView = LayoutInflater.from(getActivity()).inflate(R.layout.menu_item_layout, null);
            mMenuItem = (TextView)convertView.findViewById(R.id.menu_tv);
            String tv = getItem(position);
            mMenuItem.setText(tv);

            return convertView;
        }
    }
}
