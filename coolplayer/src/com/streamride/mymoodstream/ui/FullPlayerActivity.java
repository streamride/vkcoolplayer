package com.streamride.mymoodstream.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import com.streamride.moodstream.R;
import com.streamride.mymoodstream.ui.fragment.MoodPlaylistFragment;

/**
 * Created by andreyzakharov on 13.05.14.
 */
public class FullPlayerActivity extends ActionBarActivity {


    private ActionBar mActionbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_activity);
        initActionBar();
        initFragment();
    }

    private void initFragment(){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragment = new MoodPlaylistFragment();
        ft.replace(R.id.fragment_frame, fragment).commit();
    }

    private void initActionBar(){
        mActionbar = getSupportActionBar();
        mActionbar.setDisplayHomeAsUpEnabled(false);
        mActionbar.setHomeButtonEnabled(true);
        mActionbar.setDisplayShowTitleEnabled(false);
        mActionbar.setBackgroundDrawable(null);
        mActionbar.setTitle(getString(R.string.playlists));
    }


}
