package com.streamride.mymoodstream.ui;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import com.streamride.moodstream.R;
import com.streamride.mymoodstream.utils.C;
import com.vk.sdk.*;
import com.vk.sdk.api.VKError;

/**
 * Created by andreyzakharov on 03.04.14.
 */
public class AuthActivity extends ActionBarActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_auth_layout);
        getSupportActionBar().hide();
        VKUIHelper.onCreate(this);
        VKSdk.initialize(sdkListener, C.VK.APP_ID,VKAccessToken.tokenFromSharedPreferences(this,VKAccessToken.ACCESS_TOKEN));
        if(VKSdk.wakeUpSession()){
            startMainActivity();
        }

//        String[] fingerprint = VKUtil.getCertificateFingerprint(this, this.getPackageName());
        VKSdk.authorize(C.VK.sMyScope, true, false);
    }

    private void startMainActivity(){
        finish();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }

    @Override
    protected void onResume() {
        super.onResume();
        VKUIHelper.onResume(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VKUIHelper.onDestroy(this);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        VKUIHelper.onActivityResult(requestCode, resultCode, data);
    }

    private final VKSdkListener sdkListener = new VKSdkListener() {
        @Override
        public void onCaptchaError(VKError captchaError) {
            new VKCaptchaDialog(captchaError).show();
        }

        @Override
        public void onTokenExpired(VKAccessToken expiredToken) {
            VKSdk.authorize(C.VK.sMyScope);
        }

        @Override
        public void onAccessDenied(VKError authorizationError) {
            new AlertDialog.Builder(AuthActivity.this)
                    .setMessage(authorizationError.errorMessage)
                    .show();
        }

        @Override
        public void onReceiveNewToken(VKAccessToken newToken) {
            startMainActivity();
        }

        @Override
        public void onAcceptUserToken(VKAccessToken token) {
            startMainActivity();
        }
    };
}
