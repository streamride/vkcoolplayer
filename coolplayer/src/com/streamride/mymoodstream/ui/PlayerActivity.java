package com.streamride.mymoodstream.ui;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.Window;
import com.google.analytics.tracking.android.EasyTracker;
import com.streamride.moodstream.R;
import com.streamride.mymoodstream.ui.fragment.PlayerFragment;
import com.streamride.mymoodstream.utils.C;

/**
 * Created by andreyzakharov on 03.04.14.
 */
public class PlayerActivity extends ActionBarActivity {
    private int position;
    private ActionBar mActionbar;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        setContentView(R.layout.activity_player_layout);
        initActionBar();
        initPlayerFragment();
        position = getIntent().getIntExtra(C.PLAYER.POSITION, 0);
    }


    private void initPlayerFragment(){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        PlayerFragment fragment = PlayerFragment.newInstance(position);
        ft.replace(R.id.content_frame, fragment).commit();
    }

    private void initActionBar(){
        mActionbar = getSupportActionBar();
        mActionbar.setDisplayHomeAsUpEnabled(true);
        mActionbar.setHomeButtonEnabled(true);
        mActionbar.setDisplayShowTitleEnabled(true);
        mActionbar.setTitle(getString(R.string.playertitle));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
//                NavUtils.navigateUpFromSameTask(this);
//                return true;
//                Intent upIntent = NavUtils.getParentActivityIntent(this);
                Intent upIntent = null;
                try {
                    upIntent = NavUtils.getParentActivityIntent(this, MainActivity.class);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
//                if(upIntent == null)
//                    upIntent = new Intent(this, MainActivity.class);
                if(upIntent != null) {
                    if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                        TaskStackBuilder.create(this)
                                .addNextIntentWithParentStack(upIntent)
                                .startActivities();
                    } else
                        NavUtils.navigateUpTo(this, upIntent);
                    return true;
                }

//                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);  // Add this method.
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);  // Add this method.
    }

}