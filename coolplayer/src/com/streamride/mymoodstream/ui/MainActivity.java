package com.streamride.mymoodstream.ui;

import android.app.AlertDialog;
import android.content.*;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.*;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.*;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;
import com.appsflyer.AppsFlyerLib;
import com.google.analytics.tracking.android.EasyTracker;
import com.installtracker.InstallTracker;
import com.streamride.moodstream.R;
import com.streamride.mymoodstream.models.VkAlbum;
import com.streamride.mymoodstream.models.VkAudio;
import com.streamride.mymoodstream.network.loaders.AlbumLoader;
import com.streamride.mymoodstream.network.services.AlbumIntentService;
import com.streamride.mymoodstream.services.PlayerService;
import com.streamride.mymoodstream.ui.fragment.GroupListFragment;
import com.streamride.mymoodstream.ui.fragment.ListMusicFragment;
import com.streamride.mymoodstream.utils.*;
import com.streamride.mymoodstream.widgets.AdsWebView;
import com.vk.sdk.*;
import com.vk.sdk.api.VKError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends ActionBarActivity implements LoaderCallbacks<List<VkAlbum>>{

    private ActionBar mActionbar;
    private SearchView mSearchView;
    private int currentFragmentPosition = 0;
    private static final int MY_MUSIC = 0;
    private static final int RECOMMEND = 1;
    private static final int SAVED = 2;
    private HashMap<String, Integer> albumsHash = new HashMap<String, Integer>();


    private List<String> navigationList = new ArrayList<String>();
    private SpinnerAdapter mSpinnerAdapter;
    private RelativeLayout playerWidget;
    private ListMusicFragment listMusicFragment;
    private SensorManager sensorManager;
    private boolean isNewSong;
    private AdsWebView adsWv;
    private TextView pSong;
    private ImageButton pPlay;
    private ImageButton pNextBtn;
    private ImageButton pPrevBtn;
    private InternetStatus status;
    private MenuItem deleteAds;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;


    public enum FragmentType {
        LISTMUSIC, LISTGROUP, LISTFRIENDS, LISTPOPULAR
    }



    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        supportRequestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.main);
        VKUIHelper.onCreate(this);
        VKSdk.initialize(sdkListener, C.VK.APP_ID,VKAccessToken.tokenFromSharedPreferences(this,VKAccessToken.ACCESS_TOKEN));
        status = new InternetStatus(this);
        AppsFlyerLib.sendTracking(getApplicationContext());
        InstallTracker.sendTracking(this, "9113b06f040a90212b0a78b7f778fa44");
        if(VKSdk.wakeUpSession() && VKSdk.getAccessToken()!= null) {
            startApp();
        }
        else{
            VKSdk.authorize(C.VK.sMyScope, true, false);
        }

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
    }



    private void startApp(){
        initReceiver();
        initNavigationList();
        initActionBar();
        initMainActivity();
        playerWidget = (RelativeLayout) findViewById(R.id.player_widget_layout);
        playerWidget.setVisibility(View.GONE);
        initSmallPlayer();



        adsWv = (AdsWebView) findViewById(R.id.ads);
        adsWv.setVisibility(View.GONE);
//        if(!AdsHelper.INSTANCE.isShowAds(this)) {
//            adsWv.setVisibility(View.GONE);
//        }
//        else{
//            adsWv.setVisibility(View.VISIBLE);
//        }


        checkPlayerPlaying();
    }


    private final VKSdkListener sdkListener = new VKSdkListener() {
        @Override
        public void onCaptchaError(VKError captchaError) {
            new VKCaptchaDialog(captchaError).show();
        }

        @Override
        public void onTokenExpired(VKAccessToken expiredToken) {
            VKSdk.authorize(C.VK.sMyScope);
        }

        @Override
        public void onAccessDenied(VKError authorizationError) {
            new AlertDialog.Builder(MainActivity.this)
                    .setMessage(authorizationError.errorMessage)
                    .show();
        }

        @Override
        public void onReceiveNewToken(VKAccessToken newToken) {

        }

        @Override
        public void onAcceptUserToken(VKAccessToken token) {

        }
    };

    private void initReceiver(){
        IntentFilter filters = new IntentFilter();
        filters.addAction(C.ACTIONS.BUFFER);
        filters.addAction(C.ACTIONS.CURRENT_POSITION);
        filters.addAction(C.ACTIONS.NEW_SONG);
        filters.addAction(C.ACTIONS.PLAYER_STATE);
        registerReceiver(mMusicReceiver, filters);
    }

    private View.OnClickListener playerClickListener = new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.playBtn:
                    if(PlayerService.state == C.PLAYER_STATES.PLAYING)
                        pauseSong();
                    else if(PlayerService.state == C.PLAYER_STATES.PAUSED)
                        resumeSong();
                    break;
                case R.id.nextBtn:
//                    stopPlayer();
                    nextSong();
                    break;
                case R.id.prevBtn:
//                    stopPlayer();
                    prevSong();
                    break;
            }
        }
    };


    private void initSmallPlayer(){
        pSong = (TextView) playerWidget.findViewById(R.id.songnameTv);
        pSong.setSelected(true);
        pNextBtn = (ImageButton) playerWidget.findViewById(R.id.nextBtn);
        pPrevBtn = (ImageButton) playerWidget.findViewById(R.id.prevBtn);
        pPlay = (ImageButton) playerWidget.findViewById(R.id.playBtn);
        playerWidget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PlayerActivity.class);
                intent.putExtra(C.PLAYER.POSITION, PlayerService.currentPosition);
                startActivity(intent);
            }
        });

        pNextBtn.setOnClickListener(playerClickListener);
        pPrevBtn.setOnClickListener(playerClickListener);
        pPlay.setOnClickListener(playerClickListener);
    }



    private void resumeSong(){
        if(sendMessage(null, PlayerService.RESUME))
            listMusicFragment.updateResumeSong();
    }


    private void pauseSong(){
        if(sendMessage(null, PlayerService.PAUSE))
            listMusicFragment.updatePauseSong();
    }


    private void showPlayer(VkAudio vkAudio){
        Animation inAnimation = AnimationUtils.loadAnimation(this, R.anim.abc_slide_in_bottom);
        inAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        if(playerWidget.getVisibility() == View.GONE) {
            playerWidget.startAnimation(inAnimation);
            playerWidget.setVisibility(View.VISIBLE);
        }

        pSong.setText(vkAudio.artist + " - " + vkAudio.title );

        if(PlayerService.state == C.PLAYER_STATES.PLAYING)
            playerButtonPause();
        else
            playerButtonPlay();


//        if(status.isOnline() && AdsHelper.INSTANCE.isShowAds(this)) {
//            if(adsWv.getVisibility() == View.GONE)
//                adsWv.setVisibility(View.VISIBLE);
//            adsWv.loadUrl(C.test_url);
//            new ReloadWebView(this, adsWv);
//        }
//        else{
//            adsWv.setVisibility(View.GONE);
//        }

    }


    private void playerButtonPlay(){
        pPlay.setImageResource(R.drawable.play);
    }

    private void playerButtonPause(){
        pPlay.setBackgroundDrawable(null);
        pPlay.setImageResource(R.drawable.pause);
    }

    private void checkPlayerPlaying(){
        if(PlayerService.state == C.PLAYER_STATES.PLAYING || PlayerService.state == C.PLAYER_STATES.PREPARING){
            isNewSong = true;
            VkAudio vkAudio = PlayerService.currentSong;
            if(vkAudio != null) {
                showPlayer(vkAudio);
            }
        }
    }

    private void addStandardPlaylists(){
        navigationList.add(getResources().getString(R.string.my_music));
        navigationList.add(getResources().getString(R.string.recomendations));
        navigationList.add(getResources().getString(R.string.nav_saved));
    }

    private void initNavigationList(){
        addStandardPlaylists();

        loadMyAlbums(true);
    }

    private void initMainActivity(){
//        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
//        mDrawerToggle = new ActionBarDrawerToggle(
//                this,                  /* host Activity */
//                mDrawerLayout,         /* DrawerLayout object */
//                R.drawable.ic_launcher,  /* nav drawer icon to replace 'Up' caret */
//                R.string.drawer_open,  /* "open drawer" description */
//                R.string.drawer_close  /* "close drawer" description */
//        ) {
//
//            /** Called when a drawer has settled in a completely closed state. */
//            public void onDrawerClosed(View view) {
//                super.onDrawerClosed(view);
////                getActionBar().setTitle(mTitle);
//            }
//
//            /** Called when a drawer has settled in a completely open state. */
//            public void onDrawerOpened(View drawerView) {
//                super.onDrawerOpened(drawerView);
////                getActionBar().setTitle(mDrawerTitle);
//            }
//        };
//        mDrawerLayout.setDrawerListener(mDrawerToggle);
        String url = C.URL.apiMyMusicUrl();
        if(!TextUtils.isEmpty(url))
            setListMusicFragment(C.URL.apiMyMusicUrl(), C.PLAYLIST.MY_MUSIC, 0);
    }




    private void initActionBar(){
        mActionbar = getSupportActionBar();
        mActionbar.setDisplayShowTitleEnabled(false);
        mActionbar.setBackgroundDrawable(null);

        mActionbar.setDisplayHomeAsUpEnabled(true);
        mActionbar.setHomeButtonEnabled(true);

        mActionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        mSpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, navigationList);
        ActionBar.OnNavigationListener mOnNavigationListener = new ActionBar.OnNavigationListener() {
            // Get the same strings provided for the drop-down's ArrayAdapter



            @Override
            public boolean onNavigationItemSelected(int position, long itemId) {
                selectFragment(position);
                return true;
            }
        };
        mActionbar.setListNavigationCallbacks(mSpinnerAdapter, mOnNavigationListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        VKUIHelper.onResume(this);
        if(GlobalSettings.GLOBAL_SETTINGS.isChangeSongByShake(this)) {
            sensorManager.registerListener(mSensorEventListener,
                    sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                    SensorManager.SENSOR_DELAY_NORMAL);
        }
        if(listMusicFragment != null)
            listMusicFragment.updateSong();
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(mSensorEventListener);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VKUIHelper.onDestroy(this);
        try {
            unregisterReceiver(mMusicReceiver);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        if(mBound)
            unbindService(mConnection);
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == VKSdk.VK_SDK_REQUEST_CODE) {
            VKUIHelper.onActivityResult(requestCode, resultCode, data);
            if(VKSdk.getAccessToken() != null)
                startApp();
        }

    }


    private void setListMusicFragment(String url, String playlist, int albumId){
        listMusicFragment = (ListMusicFragment) getSupportFragmentManager().findFragmentByTag("listmusic");
        if(listMusicFragment == null) {
           FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            listMusicFragment = ListMusicFragment.newInstance(url, playlist, albumId);
            listMusicFragment.setOnStartServiceListener(new StartServiceListener(){
                @Override
                public void onStartService() {
                    Intent intent = new Intent(MainActivity.this, PlayerService.class);
        //        if(PlayerService.state == C.PLAYER_STATES.STOPPED)
//                    startService(intent);
                    bindService(intent, mConnection,Context.BIND_AUTO_CREATE);
                }
            });
           ft.replace(R.id.fragment_frame, listMusicFragment, "listmusic");
           ft.commitAllowingStateLoss();
            Intent intent = new Intent(MainActivity.this, PlayerService.class);
            //        if(PlayerService.state == C.PLAYER_STATES.STOPPED)
//                    startService(intent);
            bindService(intent, mConnection,Context.BIND_AUTO_CREATE);
       }
        else{
           listMusicFragment.update(url, playlist, albumId);
       }
    }

    private void selectFragment(int position){
        if(position == currentFragmentPosition)
            return;
        currentFragmentPosition = position;
        switch (position){
            case MY_MUSIC:
                setListMusicFragment(C.URL.apiMyMusicUrl(), C.PLAYLIST.MY_MUSIC,0);
                return;
            case RECOMMEND:
                setListMusicFragment(C.URL.apiRecommendationUrl(), C.PLAYLIST.RECOMMEND,0);
                return;
            case SAVED:
                setListMusicFragment(null, C.PLAYLIST.SAVED, 0);
                return;
        }

        String title = (String) mSpinnerAdapter.getItem(position);
        int albumId = albumsHash.get(title);
        String url = C.URL.apiMusicFromAlbumUrl(albumId);
        if(!TextUtils.isEmpty(url))
            setListMusicFragment(url, C.PLAYLIST.ALBUM_ID, albumId);
    }


//    public void openFragment(FragmentType type){
//        mDrawerLayout.closeDrawers();
//        switch (type){
//            case LISTGROUP:
//                L.debug(" list group ");
//                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                GroupListFragment listGroupFragment = GroupListFragment.newInstance();
//                ft.replace(R.id.fragment_frame, listGroupFragment, "listgroup");
//                ft.commitAllowingStateLoss();
//
//                break;
//        }
//    }


//    @Override
//    protected void onNewIntent(Intent intent) {
//        super.onNewIntent(intent);
//
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);

            mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        if(mSearchView != null) {
            mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    if (s != null && s.length() > 0) {
                        listMusicFragment.searchSong(s);
                    } else if (s != null) {
                        listMusicFragment.clearSearch();
                    }
                    return false;
                }
            });

            mSearchView.setQueryHint(getString(R.string.str_search_hint));
        }
		MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {

            @Override
            public boolean onMenuItemActionExpand(MenuItem arg0) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem arg0) {

                return true;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_settings:
                Intent intent = new Intent(MainActivity.this, PreferenceActivity.class);
                startActivity(intent);
                break;

        }
        return super.onOptionsItemSelected(item);
    }





    private void loadMyAlbums(boolean isNetworkNeed){
        Bundle bundle = new Bundle();
        bundle.putBoolean(C.LOAD.IS_NETWORK_NEED, isNetworkNeed);

        getSupportLoaderManager().restartLoader(0, bundle, this);
    }

    private void loadAlbumsFromNetwork(){
        Intent intent = new Intent(this, AlbumIntentService.class);
        intent.putExtra("url", C.URL.apiMyAlbumsUrl());
        intent.putExtra("receiver", new ResultReceiver(null) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                super.onReceiveResult(resultCode, resultData);
                if (resultCode != 0) {
                    loadMyAlbums(false);
                }
            }
        });
        startService(intent);
    }

    private void setAlbums(List<VkAlbum> albums){
        navigationList.clear();
        albumsHash.clear();
        addStandardPlaylists();
        for(VkAlbum vkAlbum: albums){
            navigationList.add(vkAlbum.title);
            albumsHash.put(vkAlbum.title, vkAlbum.album_id);
        }
    }




    private void nextSong(){
        sendMessage(null, PlayerService.NEXT);
    }

    private void prevSong(){
        sendMessage(null, PlayerService.PREV);
    }


    private boolean sendMessage(Bundle bundle, int what){
        if(!mBound)
            return false;
        Message msg = Message.obtain(null,what);
        if(bundle != null)
            msg.setData(bundle);
        try {
            mService.send(msg);
            return true;
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return false;
    }


    private Messenger mService;
    private boolean mBound;
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mService = null;
            mBound = false;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = new Messenger(service);
            mBound = true;
            checkPlayerPlaying();
        }
    };




    BroadcastReceiver mMusicReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(C.ACTIONS.NEW_SONG.equals(intent.getAction())){
                checkPlayerPlaying();
                AppsFlyerLib.sendTrackingWithEvent(getApplicationContext(), "new_song", "");
            }
            else if(C.ACTIONS.PLAYER_STATE.equals(intent.getAction())){
                int state = intent.getIntExtra(C.PLAYER.STATE,0);
                if(state == C.PLAYER_STATES.PLAYING || state == C.PLAYER_STATES.PREPARING)
                    playerButtonPause();
                else if(state == C.PLAYER_STATES.PAUSED)
                    playerButtonPlay();
            }
        }
    };


    private float mAccelCurrent;
    private float mAccelLast;
    private float mAccel;
    private final SensorEventListener mSensorEventListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];
            mAccelLast = mAccelCurrent;
            mAccelCurrent = (float) Math.sqrt((double) (x * x + y * y + z * z));
            float delta = mAccelCurrent - mAccelLast;
            mAccel = mAccelCurrent * 0.9f + delta; // perform low-cut filter

            if (mAccel > 25 && isNewSong) {
                nextSong();
                isNewSong = false;
            }
            AppsFlyerLib.sendTrackingWithEvent(getApplicationContext(), "shaked", "");
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };


    @Override
    public Loader<List<VkAlbum>> onCreateLoader(int i, Bundle bundle) {
        return new AlbumLoader(this, bundle);
    }

    @Override
    public void onLoadFinished(Loader<List<VkAlbum>> listLoader, List<VkAlbum> vkAlbums) {
        setAlbums(vkAlbums);
        AlbumLoader musicLoader = (AlbumLoader) listLoader;
        Bundle bundle = musicLoader.getBundle();
        boolean isNetworkNeed = bundle.getBoolean(C.LOAD.IS_NETWORK_NEED);
        if(isNetworkNeed)
            loadAlbumsFromNetwork();
    }

    @Override
    public void onLoaderReset(Loader<List<VkAlbum>> listLoader) {

    }

    public interface StartServiceListener {
         public void onStartService();
    }


}
