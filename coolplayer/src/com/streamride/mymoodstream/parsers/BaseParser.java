package com.streamride.mymoodstream.parsers;

import com.streamride.mymoodstream.models.VkAlbum;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by andreyzakharov on 31.07.14.
 */
public abstract class BaseParser<T> {


    public void parse(String response) throws JSONException {

        JSONObject responseObject = new JSONObject(response);
        int count = 0;
        if(responseObject.has("response")){
            JSONObject jsonObject = responseObject.getJSONObject("response");
            count = jsonObject.optInt("count");
            JSONArray jsonArray = jsonObject.optJSONArray("items");
            parseItems(count, jsonArray);
        }
    }


    protected abstract void parseItems(int count, JSONArray jsonArray);
}
