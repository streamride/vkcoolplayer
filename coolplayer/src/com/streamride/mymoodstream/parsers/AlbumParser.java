package com.streamride.mymoodstream.parsers;

import com.streamride.mymoodstream.models.VkAlbum;
import com.streamride.mymoodstream.network.impl.AlbumResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andreyzakharov on 07.04.14.
 */
public class AlbumParser {


    public AlbumResponse parse(String response) throws JSONException {
        AlbumResponse albumResponse = new AlbumResponse();
        List<VkAlbum> vkAlbumList = new ArrayList<VkAlbum>();
        int count = 0;
        JSONObject responseObject = new JSONObject(response);
        if(responseObject.has("response")){
            JSONObject jsonObject = responseObject.getJSONObject("response");
            count = jsonObject.optInt("count");
            albumResponse.count = count;
            JSONArray jsonArray = jsonObject.optJSONArray("items");
            if(jsonArray != null){
                for(int i=0;i<jsonArray.length();i++) {
                    VkAlbum vkAudio = new VkAlbum().parse(jsonArray.getJSONObject(i));
                    vkAlbumList.add(vkAudio);
                }
            }
            albumResponse.setResultList(vkAlbumList);
        }

        return albumResponse;
    }

}
