package com.streamride.mymoodstream.parsers;

import com.streamride.mymoodstream.models.VkGroup;
import com.streamride.mymoodstream.models.VkAudio;
import com.streamride.mymoodstream.network.impl.AudioResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andreyzakharov on 31.07.14.
 */
public class GroupParser {

    public List<VkGroup> parse(String response) throws JSONException {
        AudioResponse audioResponse = new AudioResponse();
        List<VkGroup> vkGroupList = new ArrayList<VkGroup>();
        int count = 0;
        JSONObject responseObject = new JSONObject(response);
        if(responseObject.has("response")){
            JSONObject jsonObject = responseObject.getJSONObject("response");
            count = jsonObject.optInt("count");
            audioResponse.count = count;
            JSONArray jsonArray = jsonObject.optJSONArray("items");
            if(jsonArray != null){
                for(int i=0;i<jsonArray.length();i++) {
                    VkGroup vkGroup = VkGroup.parse(jsonArray.getJSONObject(i));
                    vkGroupList.add(vkGroup);
                }
            }
        }


        return vkGroupList;
    }
}
