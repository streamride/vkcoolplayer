package com.streamride.mymoodstream.parsers;

import com.streamride.mymoodstream.models.VkAudio;
import com.streamride.mymoodstream.network.impl.AudioResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andreyzakharov on 04.04.14.
 */
public class MusicListParser {

    public AudioResponse parse(String response, String playlist) throws JSONException {
        AudioResponse audioResponse = new AudioResponse();
        List<VkAudio> vkAudioList = new ArrayList<VkAudio>();
        int count = 0;
        JSONObject responseObject = new JSONObject(response);
        if(responseObject.has("response")){
            JSONObject jsonObject = responseObject.getJSONObject("response");
            count = jsonObject.optInt("count");
            audioResponse.count = count;
            JSONArray jsonArray = jsonObject.optJSONArray("items");
            if(jsonArray != null){
                for(int i=0;i<jsonArray.length();i++) {
                    VkAudio vkAudio = new VkAudio().parse(jsonArray.getJSONObject(i), playlist);
                    vkAudioList.add(vkAudio);
                }
            }
            audioResponse.setResultList(vkAudioList);
        }


        return audioResponse;
    }
}
