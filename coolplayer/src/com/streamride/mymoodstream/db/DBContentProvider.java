package com.streamride.mymoodstream.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import com.streamride.mymoodstream.models.VkAlbum;
import com.streamride.mymoodstream.models.VkAudio;
import com.streamride.mymoodstream.models.VkGroup;

/**
 * Created by andreyzakharov on 03.04.14.
 */
public class DBContentProvider extends ContentProvider {


    public static final String PROVIDER = "com.streamride.moodstream";
    public static final String TABLE_AUDIO = "audio";
    public static final String TABLE_ALBUM = "album";
    public static final String TABLE_PLAYLIST = "playlist";
    public static final String TABLE_SAVED = "saved";
    public static final String TABLE_GROUP = "groups";

    public static final Uri CONTENT_URI_AUDIO = Uri.parse("content://" + PROVIDER + "/" + TABLE_AUDIO);
    public static final Uri CONTENT_URI_ALBUM = Uri.parse("content://" + PROVIDER + "/" + TABLE_ALBUM);
    public static final Uri CONTENT_URI_PLAYLIST = Uri.parse("content://" + PROVIDER + "/" + TABLE_PLAYLIST);
    public static final Uri CONTENT_URI_SAVED = Uri.parse("content://" + PROVIDER + "/" + TABLE_SAVED);
    public static final Uri CONTENT_URI_GROUP = Uri.parse("content://" + PROVIDER + "/" + TABLE_GROUP);

    private UriMatcher mUriMatcher;

    private static final int AUDIO_ID = 1;
    private static final int ALBUM_ID = 2;
    private static final int SAVED_ID = 3;
    private static final int GROUP_ID = 4;
    private SQLiteDatabase mCoolPlayerDb;


    @Override
    public boolean onCreate() {
        mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        mUriMatcher.addURI(PROVIDER, TABLE_AUDIO, AUDIO_ID);
        mUriMatcher.addURI(PROVIDER, TABLE_ALBUM, ALBUM_ID);
        mUriMatcher.addURI(PROVIDER, TABLE_SAVED, SAVED_ID);
        mUriMatcher.addURI(PROVIDER, TABLE_GROUP, GROUP_ID);

        DBHelper dbHelper = new DBHelper(getContext());
        mCoolPlayerDb = dbHelper.getWritableDatabase();

        return mCoolPlayerDb == null ? false : true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder sqLiteQueryBuilder = new SQLiteQueryBuilder();

        switch (mUriMatcher.match(uri)){
            case AUDIO_ID:
                sqLiteQueryBuilder.setTables(TABLE_AUDIO);
                break;
            case ALBUM_ID:
                sqLiteQueryBuilder.setTables(TABLE_ALBUM);
                break;
            case SAVED_ID:
                sqLiteQueryBuilder.setTables(TABLE_SAVED);
                break;
            case GROUP_ID:
                sqLiteQueryBuilder.setTables(TABLE_GROUP);
                break;
        }

        Cursor cursor = sqLiteQueryBuilder.query(mCoolPlayerDb, projection , selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        switch (mUriMatcher.match(uri)){
            case AUDIO_ID:
                return PROVIDER + "/" + TABLE_AUDIO;
            case ALBUM_ID:
                return PROVIDER + "/" + TABLE_ALBUM;
            case SAVED_ID:
                return PROVIDER + "/" + TABLE_SAVED;
            case GROUP_ID:
                return PROVIDER + "/" + TABLE_GROUP;
            default:
                throw new IllegalArgumentException("Unsupported uri " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long rowId = 0;
        Uri _uri = null;
        switch (mUriMatcher.match(uri)){
            case AUDIO_ID: {
                rowId = mCoolPlayerDb.insert(TABLE_AUDIO, "", values);
                if (rowId > 0) {
                    _uri = ContentUris.withAppendedId(CONTENT_URI_AUDIO, rowId);
                }
                break;
            }
            case ALBUM_ID: {
                rowId = mCoolPlayerDb.insert(TABLE_ALBUM, "", values);
                if (rowId > 0) {
                    _uri = ContentUris.withAppendedId(CONTENT_URI_ALBUM, rowId);
                }
                break;
            }
            case SAVED_ID: {
                rowId = mCoolPlayerDb.insert(TABLE_SAVED, "", values);
                if(rowId > 0)
                    _uri = ContentUris.withAppendedId(CONTENT_URI_SAVED, rowId);
                break;
            }
            case GROUP_ID: {
                rowId = mCoolPlayerDb.insert(TABLE_GROUP, "", values);
                if(rowId > 0)
                    _uri = ContentUris.withAppendedId(CONTENT_URI_GROUP, rowId);
                break;
            }
            default:
                throw new IllegalArgumentException("Unsupported uri "+ uri);
        }
        getContext().getContentResolver().notifyChange(_uri, null);
        return _uri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int count = 0;
        switch (mUriMatcher.match(uri)){
            case AUDIO_ID:
                count =mCoolPlayerDb.delete(TABLE_AUDIO, selection, selectionArgs);
                break;
            case ALBUM_ID:
                count=mCoolPlayerDb.delete(TABLE_ALBUM, selection, selectionArgs);
                break;
            case SAVED_ID:
                count = mCoolPlayerDb.delete(TABLE_SAVED, selection, selectionArgs);
                break;
            case GROUP_ID:
                count = mCoolPlayerDb.delete(TABLE_GROUP, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unsupported uri "+ uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);


        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int count = 0;

        switch (mUriMatcher.match(uri)){
            case AUDIO_ID:
                count = mCoolPlayerDb.update(TABLE_AUDIO, values, selection, selectionArgs);
                break;
            case ALBUM_ID:
                count = mCoolPlayerDb.update(TABLE_ALBUM, values, selection, selectionArgs);
                break;
            case SAVED_ID:
                count = mCoolPlayerDb.update(TABLE_SAVED, values, selection, selectionArgs);
                break;
            case GROUP_ID:
                count = mCoolPlayerDb.update(TABLE_GROUP, values, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unsupported uri "+ uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }


    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        switch (mUriMatcher.match(uri)){
            case AUDIO_ID:
                return bulkInsertAudio(values);
            case ALBUM_ID:
                return bulkInsertAlbums(values);
            case SAVED_ID:
                return bulkInsertSaved(values);
            case GROUP_ID:
                return bulkInsertGroups(values);

        }
        return super.bulkInsert(uri, values);
    }

    private int bulkInsertGroups(ContentValues[] values){
        int rowsAdded = 0;
        long rowId = 0;
        String selection = String.format("%s=?", VkGroup.ID);
        String[] selectionArgs = null;

        mCoolPlayerDb.beginTransaction();

        for(ContentValues cv: values){
            selectionArgs = new String[] { cv.getAsString(VkGroup.ID)};
            int updated = mCoolPlayerDb.update(TABLE_GROUP, cv, selection, selectionArgs);
            if(updated == 0) {
                rowId = mCoolPlayerDb.insert(TABLE_GROUP, null, cv);
                if(rowId > 0){
                    rowsAdded++;
                }
            }
        }
        mCoolPlayerDb.setTransactionSuccessful();
        mCoolPlayerDb.endTransaction();
        return rowsAdded;
    }

    private int bulkInsertSaved(ContentValues[] values){
        int rowsAdded = 0;
        long rowId = 0;
        String selection = String.format("%s=?", VkAudio.ID);
        String[] selectionArgs = null;

        mCoolPlayerDb.beginTransaction();

        for(ContentValues cv: values){
            selectionArgs = new String[] { cv.getAsString(VkAudio.ID)};
            int updated = mCoolPlayerDb.update(TABLE_SAVED, cv, selection, selectionArgs);
            if(updated == 0) {
                rowId = mCoolPlayerDb.insert(TABLE_SAVED, null, cv);
                if(rowId > 0){
                    rowsAdded++;
                }
            }
        }
        mCoolPlayerDb.setTransactionSuccessful();
        mCoolPlayerDb.endTransaction();
        return rowsAdded;
    }


    private int bulkInsertAudio(ContentValues[] values){
        int rowsAdded = 0;
        long rowId = 0;
        String selection = String.format("%s=?", VkAudio.ID);
        String[] selectionArgs = null;

        mCoolPlayerDb.beginTransaction();

        for(ContentValues cv: values){
            selectionArgs = new String[] { cv.getAsString(VkAudio.ID)};
            int updated = mCoolPlayerDb.update(TABLE_AUDIO, cv, selection, selectionArgs);
            if(updated == 0) {
                rowId = mCoolPlayerDb.insert(TABLE_AUDIO, null, cv);
                if(rowId > 0){
                    rowsAdded++;
                }
            }
        }
        mCoolPlayerDb.setTransactionSuccessful();
        mCoolPlayerDb.endTransaction();
        return rowsAdded;
    }

    private int bulkInsertAlbums(ContentValues[] values){
        int rowsAdded = 0;
        long rowId = 0;
        String selection = String.format("%s=?", VkAlbum.ALBUM_ID);
        String[] selectionArgs = null;

        mCoolPlayerDb.beginTransaction();

        for(ContentValues cv: values){
            selectionArgs = new String[] { cv.getAsString(VkAlbum.ALBUM_ID)};
            int updated = mCoolPlayerDb.update(TABLE_ALBUM, cv, selection, selectionArgs);
            if(updated == 0) {
                rowId = mCoolPlayerDb.insert(TABLE_ALBUM, null, cv);
                if(rowId > 0){
                    rowsAdded++;
                }
            }
        }
        mCoolPlayerDb.setTransactionSuccessful();
        mCoolPlayerDb.endTransaction();
        return rowsAdded;
    }

}
