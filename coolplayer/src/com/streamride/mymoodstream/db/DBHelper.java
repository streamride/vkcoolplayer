package com.streamride.mymoodstream.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.streamride.mymoodstream.models.Playlist;
import com.streamride.mymoodstream.models.VkAlbum;
import com.streamride.mymoodstream.models.VkAudio;
import com.streamride.mymoodstream.models.VkGroup;
import com.streamride.mymoodstream.utils.C;

import java.security.acl.Group;

/**
 * Created by andreyzakharov on 03.04.14.
 */
public class DBHelper extends SQLiteOpenHelper{


    private static final String CREATE_AUDIO_TABLE = "create table " + DBContentProvider.TABLE_AUDIO +
            " (_id integer primary key autoincrement, " + VkAudio.ID + " integer default 0, "
            + VkAudio.ALBUM_ID + " integer default 0, " + VkAudio.ARTIST + " text, " +
            VkAudio.TITLE + " text, " + VkAudio.DURATION + " integer default 0, " +
            VkAudio.GENRE + " integer default 0, " + VkAudio.LYRICS_ID + " integer default 0, " +
            VkAudio.OWNER_ID + " integer default 0, " + VkAudio.PLAYLIST + " text, "
            + VkAudio.URL + " text not null);";

    private static final String CREATE_ALBUM_TABLE = "create table " + DBContentProvider.TABLE_ALBUM +
            " (_id integer primary key autoincrement, " + VkAlbum.ALBUM_ID + " integer default 0, "
            + VkAlbum.OWNER_ID + " integer default 0, " + VkAlbum.TITLE + " text);";

    private static final String CREATE_SAVED_TABLE = "create table " + DBContentProvider.TABLE_SAVED +
            " (_id integer primary key autoincrement, " + VkAudio.ID + " integer default 0, "
            + VkAudio.ALBUM_ID + " integer default 0, " + VkAudio.ARTIST + " text, " +
            VkAudio.TITLE + " text, " + VkAudio.DURATION + " integer default 0, " +
            VkAudio.GENRE + " integer default 0, " + VkAudio.LYRICS_ID + " integer default 0, " +
            VkAudio.OWNER_ID + " integer default 0, " + VkAudio.PLAYLIST + " text, "
            + VkAudio.URL + " text not null);";

    private static final String CREATE_PLAYLIST_TABLE = "create table " + DBContentProvider.TABLE_PLAYLIST +
            " (_id integer primary key autoincrement, " + Playlist.NAME + " text not null, " + Playlist.IMG +
            " text, " + Playlist.TYPE + " text);";

    private static final String CREATE_GROUP_TABLE = "create table " + DBContentProvider.TABLE_GROUP +
            " (_id integer primary key autoincrement, " + VkGroup.ID + " integer default 0, " + VkGroup.NAME +
            " text, " + VkGroup.PHOTO_50 + " text, " + VkGroup.PHOTO_100 + " text, " + VkGroup.PHOTO_200 + " text);";

    public DBHelper(Context context){
        super(context, C.DB.DATABASE_NAME, null, C.DB.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_AUDIO_TABLE);
        db.execSQL(CREATE_ALBUM_TABLE);
        db.execSQL(CREATE_SAVED_TABLE);
        db.execSQL(CREATE_PLAYLIST_TABLE);
        db.execSQL(CREATE_GROUP_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(oldVersion < newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + DBContentProvider.TABLE_AUDIO);
            db.execSQL("DROP TABLE IF EXISTS " + DBContentProvider.TABLE_ALBUM);
            db.execSQL("DROP TABLE IF EXISTS " + DBContentProvider.TABLE_PLAYLIST);
            db.execSQL("DROP TABLE IF EXISTS " + DBContentProvider.TABLE_SAVED);
            db.execSQL("DROP TABLE IF EXISTS " + DBContentProvider.TABLE_GROUP);
            onCreate(db);
        }

    }
}
